namespace Onix.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CamposExtraPerfil : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "BrindaBuenaAtencion", c => c.Boolean());
            AddColumn("dbo.AspNetUsers", "ValidadoConFacebook", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "ValidadoConFacebook");
            DropColumn("dbo.AspNetUsers", "BrindaBuenaAtencion");
        }
    }
}
