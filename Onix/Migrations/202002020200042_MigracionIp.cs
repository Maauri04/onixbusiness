namespace Onix.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigracionIp : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "IP");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "IP", c => c.String());
        }
    }
}
