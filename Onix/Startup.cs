﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Onix.Startup))]
namespace Onix
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
