﻿using System.Web;
using System.Web.Optimization;

namespace Onix
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Content/vendor/jquery/jquery.min.js",
                      "~/Content/vendor/bootstrap/js/bootstrap.bundle.min.js",
                      "~/Content/vendor/jquery-easing/jquery.easing.min.js",
                      "~/Content/js/sb-admin-2.min.js",
                      "~/Content/js/global.js",
                      //"~/Content/vendors/bootstrap/js/bootstrap.min.js",
                      "~/Content/js/cards-index.js",
                      "~/Content/js/file-upload.js",
                      "~/Content/js/redirects.js",
                      "~/Content/js/jquery.confetti.js",
                      "~/Content/js/funciones-utiles.js",
                      "~/Content/js/jszip.min.js",
                      "~/Content/js/FileSaver.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                 "~/Content/vendors/bootstrap/css/bootstrap.min.css",
                 "~/Content/css/modern-business.css",
                 "~/Content/css/heroic-features.css",
                 "~/Content/vendor/fontawesome-free/css/all.min.css",
                 //"~/Content/css/sb-admin-2.min.css",
                 "~/Content/css/min.styles.css",
                 "~/Content/css/styles.css",
                 "~/Content/css/cards-index.css",
                 "~/Content/css/sidebar.css",
                 "~/Content/css/categories.css",
                 //"~/Content/css/file-upload.css",
                 "~/Content/css/botones-redes.css"));
        }
    }
}
