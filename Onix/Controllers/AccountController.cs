﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Datos;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Onix.Models;

namespace Onix.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        private onixDBEntities db = new onixDBEntities();

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            ViewBag.UsuarioExiste = false;
            var usuario = db.AspNetUsers.Where(x => x.Email == model.Email).FirstOrDefault();
            //var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
            //var currentUser = UserManager.FindByName(user.UserName);
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (usuario != null)
            {
                if (usuario.IdEstado == 2)
                {
                    ModelState.AddModelError("", "Usuario baneado");
                    return View(model);
                }

                //Verifica que la cuenta de correo este confirmada, si no lo esta, no permite iniciar sesion.
                if (usuario.EmailConfirmed == false)
                {
                    ModelState.AddModelError("", "¡La cuenta aún no fue confirmada! Por favor, revisa tu correo electrónico");
                    return View(model);
                }
            }

            // No cuenta los errores de inicio de sesión para el bloqueo de la cuenta
            // Para permitir que los errores de contraseña desencadenen el bloqueo de la cuenta, cambie a shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Intento de inicio de sesión no válido.");
                    return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Requerir que el usuario haya iniciado sesión con nombre de usuario y contraseña o inicio de sesión externo
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // El código siguiente protege de los ataques por fuerza bruta a los códigos de dos factores. 
            // Si un usuario introduce códigos incorrectos durante un intervalo especificado de tiempo, la cuenta del usuario 
            // se bloqueará durante un período de tiempo especificado. 
            // Puede configurar el bloqueo de la cuenta en IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Código no válido.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            ViewBag.UsuarioValido = false;
            ViewBag.UsuarioExiste = false;
            if (ModelState.IsValid)
            {
                var usuario = db.AspNetUsers.Where(x => x.Email == model.Email).FirstOrDefault();
                if (usuario == null) //No existe usuario con ese email, puede continuar.
                {
                    var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                    user.IdEstado = 1;
                    user.IdRol = 1;
                    user.Apellido = model.Apellido;
                    user.Nombre = model.Nombre;
                    user.BrindaBuenaAtencion = false;
                    user.ValidadoConFacebook = false;
                    var result = await UserManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        var currentUser = UserManager.FindByName(user.UserName);
                        UserManager.AddToRole(currentUser.Id, "Usuario");

                        // Para obtener más información sobre cómo habilitar la confirmación de cuentas y el restablecimiento de contraseña, visite https://go.microsoft.com/fwlink/?LinkID=320771
                        // Enviar correo electrónico con este vínculo
                        string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                        await UserManager.SendEmailAsync(user.Id, "Confirmar cuenta", "¡BIENVENIDO A ONIX! Para confirmar la cuenta y comenzar a publicar en el sitio, haga clic <a href=\"" + callbackUrl + "\"><b>aquí</b></a>");
                        ViewBag.UsuarioExiste = false;
                        ViewBag.UsuarioValido = true;
                        //return RedirectToAction("Index", "Home");
                    }
                    AddErrors(result);
                }
                else //El usuario existe, error
                {
                    ViewBag.UsuarioExiste = true;
                }
            }

            // Si llegamos a este punto, es que se ha producido un error y volvemos a mostrar el formulario
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // No revelar que el usuario no existe o que no está confirmado
                    return View("ForgotPasswordConfirmation");
                }

                // Para obtener más información sobre cómo habilitar la confirmación de cuentas y el restablecimiento de contraseña, visite https://go.microsoft.com/fwlink/?LinkID=320771
                // Enviar correo electrónico con este vínculo
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                await UserManager.SendEmailAsync(user.Id, "Restablecer contraseña", "Para restablecer la contraseña, haga clic <a href=\"" + callbackUrl + "\">aquí</a>");
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // Si llegamos a este punto, es que se ha producido un error y volvemos a mostrar el formulario
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // No revelar que el usuario no existe
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl, bool? ValidarUsuario)
        {
            // Solicitar redireccionamiento al proveedor de inicio de sesión externo
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl, EsValidarUsuario = ValidarUsuario }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generar el token y enviarlo
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        //ARRANCAMOS A PARTIR DE ESTA FUNCION PARA INICIAR SESION CON FACEBOOK
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl, FacebookViewModels data, bool? EsValidarUsuario)
        {
            try
            {
                byte[] imgBytes = null;
                if (data.ImagenUsuario != null)
                {
                    imgBytes = Convert.FromBase64String(data.ImagenUsuario);
                }
                //loginInfo = facebookUser;
                //var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(); la info de face ya la tengo
                SignInStatus result = SignInStatus.Failure; //Por defecto
                AspNetUsers usu = db.AspNetUsers.Where(x => x.UserName == data.EmailUsuario).FirstOrDefault();

                //INGRESA CON EL BOTON DE INICIAR CON FACEBOOK
                if (EsValidarUsuario == false || EsValidarUsuario == null)
                {
                    ApplicationUser user = new ApplicationUser();
                    AspNetUsers usuarioPrevio = db.AspNetUsers.Where(x => x.UserName == data.EmailUsuario).FirstOrDefault();
                    var loginUserActual = new UserLoginInfo("Facebook", data.IdUsuarioFacebook);
                    ExternalLoginInfo externalLoginUserActual = new ExternalLoginInfo();
                    externalLoginUserActual.Login = loginUserActual;
                    externalLoginUserActual.Email = data.EmailUsuario;
                    externalLoginUserActual.DefaultUserName = data.EmailUsuario;

                    if (usuarioPrevio != null) //tengo el usuario, le inicio sesion
                    {                       
                        AspNetUserLogins loginUsu = db.AspNetUserLogins.Where(x => x.UserId == usuarioPrevio.Id).FirstOrDefault();
                        if (loginUsu == null)
                        {
                            await UserManager.AddLoginAsync(usuarioPrevio.Id, loginUserActual);
                        }

                        
                        result = await SignInManager.ExternalSignInAsync(externalLoginUserActual, isPersistent: false);
                        user.Email = data.EmailUsuario;
                        user.Nombre = data.NombreUsuario.Split(' ')[0];
                        user.Apellido = data.NombreUsuario.Split(' ')[1];
                        string idUsuario = db.AspNetUsers.Where(x => x.UserName == user.Email).FirstOrDefault().Id;
                        ImagenesUsuarios imgUsu = db.ImagenesUsuarios.Where(x => x.IdUsuario == idUsuario).FirstOrDefault();
                        if (imgUsu != null) //Si tiene foto, se la actualizo. Sino, le creo una.
                        {
                            imgUsu.imagen = imgBytes;
                            db.Entry(imgUsu).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            ImagenesUsuarios imgUsuNueva = new ImagenesUsuarios();
                            imgUsuNueva.IdUsuario = idUsuario;
                            imgUsuNueva.imagen = imgBytes;
                            imgUsuNueva.nombre = "profile_facebook";
                            db.ImagenesUsuarios.Add(imgUsuNueva);
                            db.SaveChanges();
                        }

                        return RedirectToLocal(returnUrl, true, user.Email);
                    }
                    else //Creo el usuario y le inicio sesion
                    {
                        user.IdEstado = 1;
                        user.IdRol = 1;
                        user.Nombre = data.NombreUsuario.Split(' ')[0];
                        user.Apellido = data.NombreUsuario.Split(' ')[1];
                        user.UserName = data.EmailUsuario;
                        user.Email = data.EmailUsuario;
                        user.ValidadoConFacebook = true;
                        var resultRegistro = await UserManager.CreateAsync(user);
                        if (resultRegistro.Succeeded)
                        {
                            //Le asigno la imagen de facebook
                            string idUsuario = db.AspNetUsers.Where(x => x.UserName == user.Email).FirstOrDefault().Id;
                            ImagenesUsuarios imgUsuNueva = new ImagenesUsuarios();
                            imgUsuNueva.IdUsuario = idUsuario;
                            imgUsuNueva.imagen = imgBytes;
                            imgUsuNueva.nombre = "profile_facebook";
                            db.ImagenesUsuarios.Add(imgUsuNueva);
                            db.SaveChanges();
                            UserManager.AddToRole(user.Id, "Usuario");
                            await UserManager.AddLoginAsync(user.Id, loginUserActual);
                            result = await SignInManager.ExternalSignInAsync(externalLoginUserActual, isPersistent: false);
                            return RedirectToLocal(returnUrl, true, user.Email);
                        }
                        else
                        {
                            throw new Exception();
                        }
                    }
                }

                if (usu != null)
                {
                    //QUIERE VALIDARSE CON FACEBOOK DESDE SU PERFIL
                    bool validado = false;
                    if (data.EmailUsuario == null)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        if (EsValidarUsuario == true)
                        {
                            //INGRESO CORRECTAMENTE CON UNA CUENTA DE FACEBOOK
                            validado = true;
                            AspNetUsers usuario = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).FirstOrDefault();
                            usuario.ValidadoConFacebook = true; //Lo utilizo para guardar el campo de "Esta validado con facebook (true or false)"
                            db.Entry(usuario).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    if (EsValidarUsuario == true) //Ingresa por aca si solo estoy validandolo con facebook al usuario
                    {
                        //return RedirectToAction("Index", "Profile", new { id = usu.Id });
                        return Json(new { });
                    }
                    return RedirectToAction("Login");
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception e)
            {
                throw e;
            }           
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Configuration", "Profile"); //Index, Manage             
            }

            if (ModelState.IsValid)
            {
                // Obtener datos del usuario del proveedor de inicio de sesión externo
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                user.IdEstado = 1;
                user.IdRol = 1;
                user.Nombre = model.Nombre;
                user.Apellido = model.Apellido;
                //user.Apellido = model.Apellido; CONSULTAR POR API A FACEBOOK PARA OBTENER ESTOS DATOS (ACTIVAR PERMISO EN LA APLICACION CREADA DE FACEBOOK)
                //user.Nombre = model.Nombre;
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    var currentUser = UserManager.FindByName(user.UserName);
                    UserManager.AddToRole(currentUser.Id, "Usuario");
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl, true);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Aplicaciones auxiliares
        // Se usa para la protección XSRF al agregar inicios de sesión externos
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl, bool RegistroFace = false, string username = "")
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            TempData["RegistroConFacebook"] = RegistroFace;
            if (RegistroFace == true)
            {
                AspNetUsers usua = db.AspNetUsers.Where(x => x.UserName == username).FirstOrDefault();
                usua.ValidadoConFacebook = true;
                db.Entry(usua).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Json(new { });//Lo hago porque hice una peticion ajax al ingresar con facebook y necesita una respuesta JSON SI O SI, sino la toma como error!
            }
            return RedirectToAction("Index", "Home"); //new { RegistroConFacebook = RegistroFace}
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}