﻿using Datos;
using Onix.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Onix.Controllers
{
    public class BuscarController : Controller
    {
        private onixDBEntities db = new onixDBEntities();

        public ActionResult Index(string palabrasClave, int pagina = 1)
        {
            BuscarViewModel model = new BuscarViewModel();
            List<Posts> ListaPosts = new List<Posts>();
            ListaPosts = db.Posts.ToList();
            //Si filtra por palabras clave
            if (!string.IsNullOrEmpty(palabrasClave))
            {
                List<string> ListaPalabrasClave = new List<string>(); //Particionamos las palabras clave en una lista para buscarlas por separado
                ListaPalabrasClave = palabrasClave.Split(' ').ToList();
                List<Posts> ListaPostsAux = new List<Posts>();
                foreach (var word in ListaPalabrasClave)
                {
                    List<Posts> ListaPostsAuxWord = new List<Posts>();
                    ListaPostsAuxWord = ListaPosts.Where(x => x.Titulo.ToLower().Contains(word.ToLower()) || x.Descripcion.ToLower().Contains(word.ToLower())).OrderBy(x => x.Id).Skip((pagina - 1) * Convert.ToInt32(ConfigurationManager.AppSettings["PostsPorPagina"])).Take(Convert.ToInt32(ConfigurationManager.AppSettings["PostsPorPagina"])).ToList();
                    foreach (var item in ListaPostsAuxWord)
                    {
                        if (item != null)
                        {
                            bool existe_post = ListaPostsAux.Any(x => x.Id == item.Id);
                            if (!existe_post)
                            {
                                ListaPostsAux.Add(item);
                            }
                        }
                    }
                }
                ListaPosts = ListaPostsAux;
            }
            
            model.TotalDeRegistros = db.Posts.Count();
            model.PaginaActual = pagina;
            model.RegistrosPorPagina = Convert.ToInt32(ConfigurationManager.AppSettings["PostsPorPagina"]);

            List<PostExtendedViewModels> listaPosts = new List<PostExtendedViewModels>();
            foreach (var post in ListaPosts)
            {
                Imagenes imgPost = db.Imagenes.Where(x => x.IdPost == post.Id && x.portada == true).FirstOrDefault();
                if (imgPost != null)
                {
                    PostExtendedViewModels modelPost = new PostExtendedViewModels();
                    modelPost.IdPost = post.Id;
                    modelPost.Precio = post.Precio;
                    modelPost.Moneda = post.Moneda;
                    modelPost.Descripcion = post.Descripcion;
                    modelPost.Imagen = imgPost.imagen;
                    modelPost.Titulo = post.Titulo;
                    modelPost.PermiteCanjes = post.PermiteCanjes;
                    modelPost.PermiteContraOfertas = post.PermiteContraofertas;
                    modelPost.PermiteCuotas = post.PermiteCuotas;
                    modelPost.Prioridad = post.Prioridad;
                    modelPost.Fecha = post.FechaPublicacion;
                    listaPosts.Add(modelPost);
                }
                else
                {
                    PostExtendedViewModels modelPost = new PostExtendedViewModels();
                    modelPost.IdPost = post.Id;
                    modelPost.Precio = post.Precio;
                    modelPost.Moneda = post.Moneda;
                    modelPost.Descripcion = post.Descripcion;
                    modelPost.Imagen = null;
                    modelPost.Titulo = post.Titulo;
                    modelPost.PermiteCanjes = post.PermiteCanjes;
                    modelPost.PermiteContraOfertas = post.PermiteContraofertas;
                    modelPost.PermiteCuotas = post.PermiteCuotas;
                    modelPost.Prioridad = post.Prioridad;
                    modelPost.Fecha = post.FechaPublicacion;
                    listaPosts.Add(modelPost);
                }
            }

            model.palabrasClave = palabrasClave;
            model.ListaPosts = listaPosts;
            model.ListaPosts = model.ListaPosts.OrderByDescending(x => x.Prioridad).ThenByDescending(x => x.Fecha).ToList();
            return View(model);
        }
    }
}