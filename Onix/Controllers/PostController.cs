﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Onix.Models;
using System.IO;
using MercadoPago;
using MercadoPago.Resources;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using System.Net.PeerToPeer;
using System.Net;
using ObjectDumper;
using System.Drawing;
using System.Drawing.Imaging;
using System.Security.Principal;

namespace Onix.Controllers
{
    public class PostController : Controller
    {
        private onixDBEntities db = new onixDBEntities();
        private static List<ImagenViewModels> listaImagenesPosts = new List<ImagenViewModels>();
        private static List<ImagenViewModels> listaImagenesCanjes = new List<ImagenViewModels>();
        private static string IdImagen = "";
        private static string pathOpt = "";
        private static byte[] ImagenDestacada = null;
        private static string TituloAviso = null;

        // GET: Post
        public ActionResult Index(int id)
        {
            try
            {
                //recupero img guardadas por un canje anterior no confirmado
                List<string> files_routes = new List<string>();
                if (listaImagenesCanjes.Count() > 0)
                {
                    foreach (var item in listaImagenesCanjes)
                    {
                        files_routes.Add(item.Nombre);
                    }
                    ViewBag.FilesRoute = files_routes;
                }

                List<Imagenes> listaImg = new List<Imagenes>();
                List<Comentarios> listaComUsuarios = new List<Comentarios>();
                listaComUsuarios = db.Comentarios.Where(x => x.IdPost == id && x.IdEstado == 1).ToList();
                var post = db.Posts.Where(x => x.Id == id && x.IdEstado == 1).First();
                PostViewModels model = new PostViewModels();
                model.IdPost = post.Id;
                model.IdUsuario = post.IdUsuario;
                model.ApellidoUsuario = post.ApellidoUsuario;
                model.Celular = post.Celular;
                model.Descripcion = post.Descripcion;
                listaImg = db.Imagenes.Where(x => x.IdPost == id).ToList();
                foreach (var item in listaImg)
                {
                    model.ListaImagenes.Add(item.imagen);
                }
                model.Direccion = post.Direccion;
                model.Email = post.Email;
                model.Fecha = post.FechaPublicacion;
                model.Localidad = post.Localidad;
                model.LocalidadContacto = post.LocalidadContacto;
                model.Moneda = post.Moneda;
                model.NombreUsuario = post.NombreUsuario;
                model.PermiteCanjes = post.PermiteCanjes;
                model.PermiteContraofertas = post.PermiteContraofertas;
                model.PermiteCuotas = post.PermiteCuotas;
                model.PermitePreguntas = post.PermitePreguntas;
                model.Precio = post.Precio;
                model.TelefonoFijo = post.TelefonoFijo;
                model.Titulo = post.Titulo;
                model.Visitas = post.Visitas;
                model.Categoria = db.Categorias.Where(x => x.Id == post.IdCategoria).First().Descripcion;
                model.SubCategoria = db.SubCategorias.Where(x => x.Id == post.IdSubCategoria).First().Descripcion;
                model.ApellidoVisible = (bool)post.ApellidoUsuarioVisible;
                model.EmailVisible = (bool)post.EmailVisible;
                model.CelularVisible = (bool)post.CelularVisible;
                model.TelefonoVisible = (bool)post.TelefonoFijoVisible;
                model.DomicilioVisible = (bool)post.DireccionVisible;
                model.LocalidadVisible = (bool)post.LocalidadContactoVisible;
                model.CantidadPublicaciones = db.Posts.Where(x => x.IdUsuario == post.IdUsuario && x.IdEstado == 1).Count();
                model.Prioridad = post.Prioridad;
                List<Votos> votosRecibidosUsu = db.Votos.Where(x => x.IdUsuarioDestino == post.IdUsuario).ToList();
                if (votosRecibidosUsu.Count() > 0)
                {
                    int votosPositivosUsu = votosRecibidosUsu.Where(x => x.VotoPositivo == true).Count();
                    decimal valoracionesPositivas = (votosPositivosUsu * 100) / votosRecibidosUsu.Count();
                    model.ValoracionesPositivas = Convert.ToInt32(valoracionesPositivas);
                }
                else
                {
                    model.ValoracionesPositivas = -1;
                }
                switch (model.Categoria)
                {
                    case "Automotores":
                        model.Automotores = db.CaracteristicasAutomotores.Where(x => x.IdPost == id).First();
                        break;
                    case "Agro":
                        model.Agro = db.CaracteristicasAgro.Where(x => x.IdPost == id).First();
                        break;
                    case "Animales":
                        model.Animales = db.CaracteristicasAnimales.Where(x => x.IdPost == id).First();
                        break;
                    case "Bebes y Niños":
                        model.BebesNinos = db.CaracteristicasBebesNinos.Where(x => x.IdPost == id).First();
                        break;
                    case "Electrónica":
                        model.Electronica = db.CaracteristicasElectronica.Where(x => x.IdPost == id).First();
                        break;
                    case "Empleos":
                        model.Empleos = db.CaracteristicasEmpleos.Where(x => x.IdPost == id).First();
                        break;
                    case "Hogar y Muebles":
                        model.HogarMuebles = db.CaracteristicasHogarMuebles.Where(x => x.IdPost == id).First();
                        break;
                    case "Inmuebles":
                        model.Inmuebles = db.CaracteristicasInmuebles.Where(x => x.IdPost == id).First();
                        break;
                    case "Varios":
                        model.Varios = db.CaracteristicasVarios.Where(x => x.IdPost == id).First();
                        break;
                    default:
                        break;
                }
                AspNetUsers usuario = new AspNetUsers();
                usuario = db.AspNetUsers.Where(x => x.Id == post.IdUsuario).FirstOrDefault();
                ImagenesUsuarios imgUsuarioPost = db.ImagenesUsuarios.Where(x => x.IdUsuario == usuario.Id).FirstOrDefault();
                if (imgUsuarioPost != null)
                {
                    model.ImagenUsuario = imgUsuarioPost.imagen;
                }
                else
                {
                    model.ImagenUsuario = null;
                }
                List<ImagenesUsuarios> ListaImgUsuarios = db.ImagenesUsuarios.ToList();
                List<ComentarioViewModels> comentarios = new List<ComentarioViewModels>();
                foreach (var com in listaComUsuarios)
                {
                    bool tiene_imagen = false;
                    foreach (var imgusu in ListaImgUsuarios)
                    {
                        if (com.IdUsuario == imgusu.IdUsuario)
                        {
                            tiene_imagen = true;
                            comentarios.Add(new ComentarioViewModels
                            {
                                Imagen = imgusu.imagen,
                                Nombre = com.AspNetUsers.Nombre,
                                Contenido = com.Contenido,
                                Fecha = com.Fecha,
                                EsVendedor = com.IdUsuario == model.IdUsuario ? true : false
                            });
                            break;
                        }
                    }
                    if (!tiene_imagen)
                    {
                        comentarios.Add(new ComentarioViewModels
                        {
                            Imagen = null,
                            Nombre = com.AspNetUsers.Nombre,
                            Contenido = com.Contenido,
                            Fecha = com.Fecha,
                            EsVendedor = com.IdUsuario == model.IdUsuario ? true : false
                        });
                    }
                }
                model.ListaComentarios = comentarios;
                List<Contraofertas> ListaContraofertas = new List<Contraofertas>();
                ListaContraofertas = db.Contraofertas.Where(x => x.IdPost == post.Id && x.EstadoContraoferta == "Activa").ToList();
                List<Canjes> ListaCanjes = new List<Canjes>();
                ListaCanjes = db.Canjes.Where(x => x.IdPost == post.Id && x.EstadoCanje == "Activo").ToList();

                foreach (var contraoferta in ListaContraofertas)
                {
                    ContraOfertasViewModels ofertaModel = new ContraOfertasViewModels();
                    ofertaModel.Id = contraoferta.Id;
                    ofertaModel.IdPost = contraoferta.IdPost;
                    AspNetUsers persona = new AspNetUsers();
                    persona = db.AspNetUsers.Where(x => x.Id == contraoferta.IdUsuario).FirstOrDefault();
                    ofertaModel.IdUsuario = persona.Id;
                    ofertaModel.NombreUsuario = persona.Nombre;
                    ofertaModel.ApellidoUsuario = persona.Apellido;
                    ofertaModel.Precio = contraoferta.Precio;
                    model.ListaContraOfertas.Add(ofertaModel);
                }
                foreach (var canje in ListaCanjes)
                {
                    CanjesViewModels canjeModel = new CanjesViewModels();
                    canjeModel.Id = canje.Id;
                    canjeModel.IdPost = canje.IdPost;
                    AspNetUsers persona = new AspNetUsers();
                    persona = db.AspNetUsers.Where(x => x.Id == canje.IdUsuario).FirstOrDefault();
                    canjeModel.IdUsuario = persona.Id;
                    canjeModel.NombreUsuario = persona.Nombre;
                    canjeModel.ApellidoUsuario = persona.Apellido;
                    canjeModel.Precio = canje.Precio;
                    canjeModel.ListaImagenesCanje = db.ImagenesCanjes.Where(x => x.IdUsuario == persona.Id && x.IdPost == id).ToList();
                    canjeModel.Descripcion = canje.Descripcion;
                    model.ListaCanjes.Add(canjeModel);
                }
                ViewBag.TieneOfertasAprobadas = false;
                if (User.Identity.IsAuthenticated)
                {
                    AspNetUsers usuarioActual = db.AspNetUsers.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
                    Contraofertas tieneContraofertaAceptada = db.Contraofertas.Where(x => x.IdUsuario == usuarioActual.Id && x.IdPost == id && x.EstadoContraoferta == "Aceptada").FirstOrDefault();
                    if (tieneContraofertaAceptada != null)
                    {
                        ViewBag.TieneOfertasAprobadas = true;
                    }
                    Canjes tieneCanjeAceptado = db.Canjes.Where(x => x.IdUsuario == usuarioActual.Id && x.IdPost == id && x.EstadoCanje == "Aceptado").FirstOrDefault();
                    if (tieneCanjeAceptado != null)
                    {
                        ViewBag.TieneOfertasAprobadas = true;
                    }
                }

                //Actualizo visitas del post
                if (User.Identity.Name != usuario.UserName)
                {
                    post.Visitas += 1;
                    db.Entry(post).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                else
                {
                    if (post.Visitas == 0)
                    {
                        ViewBag.Dest = true; //Activamos el cartel de "¿Queres destacar tu aviso?"
                        post.Visitas += 1;
                        db.Entry(post).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                return View(model);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public ActionResult GuardarContraOferta(ContraofertaViewModel data)
        {
            try
            {
                string mensaje = "";
                bool exito = false;
                //Compruebo si el vendedor ya acepto alguna oferta o canje
                Contraofertas ContraOfertaAceptada = db.Contraofertas.Where(x => x.IdPost == data.IdPost && x.EstadoContraoferta == "Aceptada").FirstOrDefault();
                if (ContraOfertaAceptada == null) //No tiene contraofertas aceptadas
                {
                    Canjes CanjeAceptado = db.Canjes.Where(x => x.IdPost == data.IdPost && x.EstadoCanje == "Aceptado").FirstOrDefault();
                    if (CanjeAceptado == null)
                    {
                        //--COMPRUEBO QUE NO ESTE ESA CONTRAOFERTA ACTIVA-- //Activa o Rechazada
                        AspNetUsers usuarioOfertante = db.AspNetUsers.Where(x => x.UserName == data.IdUsuario).FirstOrDefault();
                        Contraofertas ContraOfertaActual = db.Contraofertas.Where(x => x.IdPost == data.IdPost && x.IdUsuario == usuarioOfertante.Id).FirstOrDefault(); //Solo un registro de contraoferta por usuario
                        if (ContraOfertaActual != null) //Hay una contraoferta por ese usuario
                        {
                            if (ContraOfertaActual.EstadoContraoferta == "Activa")
                            {
                                mensaje = "¡Ya tienes una contraoferta activa en esta publicación! Espera a que el vendedor responda por ella.";
                            }
                            else
                            {
                                if (ContraOfertaActual.EstadoContraoferta == "Aceptada")
                                {
                                    mensaje = "¡Ya tienes una contraoferta aceptada en esta publicación! Envíale un mensaje al vendedor para finalizar la venta.";
                                }
                                else //Contraoferta RECHAZADA
                                {
                                    //CONTROLAR CUANTAS CONTRAOFERTAS VAN PARA LIMITARLO
                                    if (data.PrecioOfertado < ContraOfertaActual.Precio)
                                    {
                                        ContraOfertaActual.Precio = data.PrecioOfertado;
                                        ContraOfertaActual.EstadoContraoferta = "Activa";
                                        db.Entry(ContraOfertaActual).State = System.Data.Entity.EntityState.Modified;
                                        db.SaveChanges();

                                        //Enviar notificacion al vendedor
                                        Notificaciones notify = new Notificaciones();
                                        notify.IdUsuarioOrigen = db.AspNetUsers.Where(x => x.UserName == data.IdUsuario).FirstOrDefault().Id;
                                        notify.IdUsuarioDestino = db.Posts.Where(x => x.Id == data.IdPost).FirstOrDefault().IdUsuario;
                                        notify.Fecha = DateTime.Now;
                                        notify.IdTipo = 1;
                                        notify.Leida = false;
                                        notify.URL = "/Post/Index/" + data.IdPost;
                                        notify.Descripcion = "¡Recibiste una contraoferta en tu publicación!";
                                        db.Notificaciones.Add(notify);
                                        db.SaveChanges();

                                        exito = true;
                                        mensaje = "Recibirás una notificación cuando el vendedor responda a tu oferta.";
                                    }
                                    else
                                    {
                                        mensaje = "¡Tu contraoferta actual debe ser más baja que la realizada anteriormente! (" + ContraOfertaActual.Precio + ")";
                                    }
                                }
                            }
                        }
                        else //No hay contraoferta por ese usuario. Puede ofertar
                        {
                            if (data.PrecioOfertado < data.PrecioPost)
                            {
                                Contraofertas co = new Contraofertas();
                                co.IdPost = data.IdPost;
                                co.IdUsuario = db.AspNetUsers.Where(x => x.UserName == data.IdUsuario).FirstOrDefault().Id;
                                co.Precio = data.PrecioOfertado;
                                co.EstadoContraoferta = data.EstadoContraOferta;
                                db.Contraofertas.Add(co);
                                db.SaveChanges();

                                //Enviar notificacion al vendedor
                                Notificaciones notify = new Notificaciones();
                                notify.IdUsuarioOrigen = db.AspNetUsers.Where(x => x.UserName == data.IdUsuario).FirstOrDefault().Id;
                                notify.IdUsuarioDestino = db.Posts.Where(x => x.Id == data.IdPost).FirstOrDefault().IdUsuario;
                                notify.Fecha = DateTime.Now;
                                notify.IdTipo = 1;
                                notify.Leida = false;
                                notify.URL = "/Post/Index/" + data.IdPost;
                                notify.Descripcion = "¡Recibiste una contraoferta en tu publicación!";
                                db.Notificaciones.Add(notify);
                                db.SaveChanges();

                                exito = true;
                                mensaje = "Recibirás una notificación cuando el vendedor responda a tu oferta.";
                            }
                            else
                            {
                                exito = false;
                                mensaje = "¡El precio ofertado no es menor al precio de la publicación!";
                            }
                        }
                    }
                    else
                    {
                        exito = false;
                        mensaje = "¡Lo sentimos! El usuario ya aceptó otra oferta.. ";
                    }
                }
                else
                {
                    exito = false;
                    mensaje = "¡Lo sentimos! El usuario ya aceptó otra oferta.. ";
                }

                return Json(new { mensaje, exito });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public async Task<ActionResult> ActualizarContraOferta(ActualizarContraOfertaViewModels data)
        {
            try
            {
                Contraofertas ContraOfertaActual = db.Contraofertas.Where(x => x.Id == data.idContraoferta).FirstOrDefault(); //Solo un registro de contraoferta por usuario
                Notificaciones notify = new Notificaciones();
                notify.IdUsuarioOrigen = db.AspNetUsers.Where(x => x.UserName == data.userNameVendedor).FirstOrDefault().Id;
                notify.IdUsuarioDestino = ContraOfertaActual.IdUsuario;
                notify.Fecha = DateTime.Now;
                notify.IdTipo = 1; //Contraoferta
                notify.Leida = false;
                notify.URL = "/Post/Index/" + ContraOfertaActual.IdPost;
                if (data.aceptaContraoferta)
                {
                    //Se rechazan las demas contraofertas y ofertas de canjes que tiene el vendedor
                    List<Contraofertas> contraofertasPost = db.Contraofertas.Where(x => x.IdPost == ContraOfertaActual.IdPost && x.IdUsuario != ContraOfertaActual.IdUsuario).ToList();
                    foreach (var item in contraofertasPost)
                    {
                        item.EstadoContraoferta = "Rechazada";
                        db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        //enviar notif al usuario
                        Notificaciones notifUsuRechazadoOfe = new Notificaciones();
                        notifUsuRechazadoOfe.IdUsuarioOrigen = notify.IdUsuarioOrigen;
                        notifUsuRechazadoOfe.IdUsuarioDestino = item.IdUsuario;
                        notifUsuRechazadoOfe.Fecha = DateTime.Now;
                        notifUsuRechazadoOfe.IdTipo = 1; //Contraoferta
                        notifUsuRechazadoOfe.Leida = false;
                        notifUsuRechazadoOfe.URL = "/Post/Index/" + ContraOfertaActual.IdPost;
                        notifUsuRechazadoOfe.Descripcion = "¡El vendedor rechazó tu oferta!";
                        db.Notificaciones.Add(notifUsuRechazadoOfe);
                    }
                    db.SaveChanges();
                    List<Canjes> canjesPost = db.Canjes.Where(x => x.IdPost == ContraOfertaActual.IdPost && x.IdUsuario != ContraOfertaActual.IdUsuario).ToList();
                    foreach (var item in canjesPost)
                    {
                        item.EstadoCanje = "Rechazado";
                        db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        //enviar notif al usuario
                        Notificaciones notifUsuRechazadoCanje = new Notificaciones();
                        notifUsuRechazadoCanje.IdUsuarioOrigen = notify.IdUsuarioOrigen;
                        notifUsuRechazadoCanje.IdUsuarioDestino = item.IdUsuario;
                        notifUsuRechazadoCanje.Fecha = DateTime.Now;
                        notifUsuRechazadoCanje.IdTipo = 2; //Contraoferta
                        notifUsuRechazadoCanje.Leida = false;
                        notifUsuRechazadoCanje.URL = "/Post/Index/" + ContraOfertaActual.IdPost;
                        notifUsuRechazadoCanje.Descripcion = "¡El vendedor rechazó tu oferta!";
                        db.Notificaciones.Add(notifUsuRechazadoCanje);
                    }
                    db.SaveChanges();

                    ContraOfertaActual.EstadoContraoferta = "Aceptada";
                    notify.Descripcion = "¡El vendedor aceptó tu contraoferta!";
                    //ENVIAR MENSAJE AL COMPRADOR POR MAIL AVISANDO QUE EL TRATO FUE CERRADO
                    var callbackUrl = Url.Action("Index", "Post", new { id = ContraOfertaActual.IdPost }, protocol: Request.Url.Scheme);
                    await UserManager.SendEmailAsync(ContraOfertaActual.IdUsuario, "¡Trato cerrado! - ONIX RAFAELA", "¡El vendedor " + data.userNameVendedor + " aceptó tu contraoferta de " + "$" + ContraOfertaActual.Precio + "! Ya podes coordinar con el vendedor para concretar la venta. Para ingresar a la publicación de ONIX, hacé clic <a href=\"" + callbackUrl + "\"><b>ACÁ</b></a>");
                }
                else
                {
                    ContraOfertaActual.EstadoContraoferta = "Rechazada";
                    notify.Descripcion = "¡El vendedor rechazó tu contraoferta!";
                }
                db.Notificaciones.Add(notify);
                db.Entry(ContraOfertaActual).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                return Json(new { });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public async Task<ActionResult> ActualizarCanje(ActualizarCanjeViewModels data)
        {
            try
            {
                Canjes CanjeActual = db.Canjes.Where(x => x.Id == data.idCanje).FirstOrDefault(); //Solo un registro de canje por usuario
                Notificaciones notify = new Notificaciones();
                notify.IdUsuarioOrigen = db.AspNetUsers.Where(x => x.UserName == data.userNameVendedor).FirstOrDefault().Id;
                notify.IdUsuarioDestino = CanjeActual.IdUsuario;
                notify.Fecha = DateTime.Now;
                notify.IdTipo = 2; //Canje
                notify.Leida = false;
                notify.URL = "/Post/Index/" + CanjeActual.IdPost;
                if (data.aceptaCanje)
                {
                    //Se rechazan las demas contraofertas y ofertas de canjes que tiene el vendedor
                    List<Contraofertas> contraofertasPost = db.Contraofertas.Where(x => x.IdPost == CanjeActual.IdPost && x.IdUsuario != CanjeActual.IdUsuario).ToList();
                    foreach (var item in contraofertasPost)
                    {
                        item.EstadoContraoferta = "Rechazada";
                        db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        //enviar notif al usuario
                        Notificaciones notifUsuRechazadoOfe = new Notificaciones();
                        notifUsuRechazadoOfe.IdUsuarioOrigen = notify.IdUsuarioOrigen;
                        notifUsuRechazadoOfe.IdUsuarioDestino = item.IdUsuario;
                        notifUsuRechazadoOfe.Fecha = DateTime.Now;
                        notifUsuRechazadoOfe.IdTipo = 1; //Contraoferta
                        notifUsuRechazadoOfe.Leida = false;
                        notifUsuRechazadoOfe.URL = "/Post/Index/" + CanjeActual.IdPost;
                        notifUsuRechazadoOfe.Descripcion = "¡El vendedor rechazó tu oferta!";
                        db.Notificaciones.Add(notifUsuRechazadoOfe);
                    }
                    db.SaveChanges();
                    List<Canjes> canjesPost = db.Canjes.Where(x => x.IdPost == CanjeActual.IdPost && x.IdUsuario != CanjeActual.IdUsuario).ToList();
                    foreach (var item in canjesPost)
                    {
                        item.EstadoCanje = "Rechazado";
                        db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                        //enviar notif al usuario
                        Notificaciones notifUsuRechazadoCanje = new Notificaciones();
                        notifUsuRechazadoCanje.IdUsuarioOrigen = notify.IdUsuarioOrigen;
                        notifUsuRechazadoCanje.IdUsuarioDestino = item.IdUsuario;
                        notifUsuRechazadoCanje.Fecha = DateTime.Now;
                        notifUsuRechazadoCanje.IdTipo = 2; //Contraoferta
                        notifUsuRechazadoCanje.Leida = false;
                        notifUsuRechazadoCanje.URL = "/Post/Index/" + CanjeActual.IdPost;
                        notifUsuRechazadoCanje.Descripcion = "¡El vendedor rechazó tu oferta!";
                        db.Notificaciones.Add(notifUsuRechazadoCanje);
                    }
                    db.SaveChanges();

                    CanjeActual.EstadoCanje = "Aceptado";
                    notify.Descripcion = "¡El vendedor aceptó tu contraoferta!";
                    //ENVIAR MENSAJE AL COMPRADOR POR WP,SMS Y MAIL AVISANDO QUE EL TRATO FUE CERRADO
                    var callbackUrl = Url.Action("Index", "Post", new { id = CanjeActual.IdPost }, protocol: Request.Url.Scheme);
                    if (CanjeActual.CanjeParcial)
                    {
                        await UserManager.SendEmailAsync(CanjeActual.IdUsuario, "¡Trato cerrado! - ONIX RAFAELA", "¡El vendedor " + data.userNameVendedor + " aceptó tu oferta de canje: " + CanjeActual.Descripcion + " + " + "$" + CanjeActual.Precio + "! Ya podes coordinar con el vendedor para concretar el canje. Para ingresar a la publicación de ONIX, hacé clic <a href=\"" + callbackUrl + "\"><b>ACÁ</b></a>");
                    }
                    else
                    {
                        await UserManager.SendEmailAsync(CanjeActual.IdUsuario, "¡Trato cerrado! - ONIX RAFAELA", "¡El vendedor " + data.userNameVendedor + " aceptó tu oferta de canje: " + CanjeActual.Descripcion + " (canje directo - " + "$" + CanjeActual.Precio + ")" + "! Ya podes coordinar con el vendedor para concretar el canje. Para ingresar a la publicación de ONIX, hacé clic <a href=\"" + callbackUrl + "\"><b>ACÁ</b></a>");
                    }
                }
                else
                {
                    CanjeActual.EstadoCanje = "Rechazado";
                    notify.Descripcion = "¡El vendedor rechazó tu oferta de canje!";
                }
                db.Notificaciones.Add(notify);
                db.Entry(CanjeActual).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                return Json(new { });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public ActionResult GuardarContraCanje(ContracanjeViewModel data) //1 SOLO CONTRACANJE DISPONIBLE.
        {
            try
            {
                DeleteImagesFromServer(false, null);
                string mensaje = "";
                bool exito = false;
                AspNetUsers usuarioOfertante = db.AspNetUsers.Where(x => x.UserName == data.IdUsuario).FirstOrDefault();
                //--COMPRUEBO QUE NO ESTE ESE CONTRACANJE ACTIVO-- //Activa, Rechazada o Aceptada
                Canjes ContraCanjeActual = db.Canjes.Where(x => x.IdPost == data.IdPost && x.IdUsuario == usuarioOfertante.Id).FirstOrDefault();
                if (ContraCanjeActual != null) //Hay un contracanje por ese usuario
                {
                    if (ContraCanjeActual.EstadoCanje == "Activo")
                    {
                        mensaje = "¡Ya tienes un canje activo en esta publicación! Espera a que el vendedor responda por el.";
                    }
                    else
                    {
                        if (ContraCanjeActual.EstadoCanje == "Aceptado")
                        {
                            mensaje = "¡Ya tienes canje aceptado en esta publicación! Envíale un mensaje al vendedor para finalizar la entrega de ambos productos";
                        }
                        else
                        {
                            mensaje = "¡Ya has enviado un canje y no fue aceptado en esta publicación! No se permiten mas intentos, intenta con una contraoferta de precio!";
                        }
                    }
                }
                else //No hay contracanje por ese usuario. Puede ofertar
                {
                    Canjes ca = new Canjes();
                    ca.IdPost = data.IdPost;
                    ca.IdUsuario = db.AspNetUsers.Where(x => x.UserName == data.IdUsuario).FirstOrDefault().Id;
                    ca.Precio = data.Precio;
                    ca.EstadoCanje = data.EstadoCanje;
                    ca.Descripcion = data.Descripcion;
                    ca.CanjeParcial = data.CanjeParcial;
                    db.Canjes.Add(ca);
                    db.SaveChanges();

                    if (listaImagenesCanjes.Count > 0)
                    {
                        for (int i = 0; i < listaImagenesCanjes.Count; i++)
                        {
                            ImagenesCanjes images = new ImagenesCanjes
                            {
                                IdUsuario = ca.IdUsuario,
                                imagen = listaImagenesCanjes[i].Imagen,
                                nombre = listaImagenesCanjes[i].Nombre,
                                IdPost = ca.IdPost
                            };
                            db.ImagenesCanjes.Add(images);
                        }
                        db.SaveChanges();
                    }

                    //Enviar notificacion al vendedor
                    Notificaciones notify = new Notificaciones();
                    notify.IdUsuarioOrigen = db.AspNetUsers.Where(x => x.UserName == data.IdUsuario).FirstOrDefault().Id;
                    notify.IdUsuarioDestino = db.Posts.Where(x => x.Id == data.IdPost).FirstOrDefault().IdUsuario;
                    notify.Fecha = DateTime.Now;
                    notify.IdTipo = 2;
                    notify.Leida = false;
                    notify.URL = "/Post/Index/" + data.IdPost;
                    notify.Descripcion = "¡Recibiste una oferta de canje en tu publicación!";
                    db.Notificaciones.Add(notify);
                    db.SaveChanges();

                    exito = true;
                    mensaje = "Recibirás una notificación cuando el vendedor responda a tu oferta de canje.";
                }
                listaImagenesCanjes.Clear();
                return Json(new { mensaje, exito });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public ActionResult GuardarComentario(ComentarioViewModel data) 
        {
            try
            {
                string mensaje = "";
                bool exito = false;
                string idusu = db.AspNetUsers.Where(x => x.UserName == data.UserName).First().Id;
                int CantComentarios = db.Comentarios.Where(x => x.IdPost == data.IdPost && x.IdUsuario == idusu).Count();
                if (CantComentarios < 15) //Limito cantidad de comentarios en la publicacion por ese usuario
                {
                    //Enviar notificacion al vendedor
                    Comentarios comentario = new Comentarios();
                    comentario.IdPost = data.IdPost;
                    comentario.IdUsuario = idusu;
                    comentario.IdEstado = data.IdEstado;
                    comentario.Contenido = data.Contenido;
                    comentario.Fecha = DateTime.Now;
                    db.Comentarios.Add(comentario);
                    db.SaveChanges();

                    //Enviar notificacion al vendedor
                    Notificaciones notify = new Notificaciones();
                    notify.IdUsuarioOrigen = idusu;
                    notify.IdUsuarioDestino = db.Posts.Where(x => x.Id == data.IdPost).FirstOrDefault().IdUsuario;
                    notify.Fecha = DateTime.Now;
                    notify.IdTipo = 3;
                    notify.Leida = false;
                    notify.URL = "/Post/Index/" + data.IdPost;
                    notify.Descripcion = "¡Recibiste un comentario en tu publicación!";
                    db.Notificaciones.Add(notify);
                    db.SaveChanges();

                    exito = true;
                    mensaje = "¡Recibirás una respuesta pronto!";
                }
                else
                {
                    exito = false;
                    mensaje = "Parece que has alcanzado el número máximo de comentarios permitidos en esta publicación. Por favor, presioná en el botón 'Chatear con el vendedor' para hablar directamente con el vendedor.";
                }
                return Json(new { mensaje, exito });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpPost]
        public ActionResult GuardarDatosExtraUsuario(DatosExtraUsuViewModel data)
        {
            try
            {
                string mensaje = "";
                bool exito = false;
                AspNetUsers usu = db.AspNetUsers.Where(x => x.UserName == data.IdUsuario).FirstOrDefault();
                if (usu != null) 
                {
                    usu.Direccion = data.Direccion;
                    usu.PhoneNumber = data.Celular;
                    usu.Localidad = data.Localidad;
                    usu.PhoneNumberConfirmed = true;
                    db.Entry(usu).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    exito = true;
                }
                else
                {
                    exito = false;
                    mensaje = "Ha ocurrido un error. Por favor, intenta cargar tus datos más tarde desde tu perfil.";
                }
                return Json(new { mensaje, exito });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ActionResult CaracteristicasProducto(string category)
        {

            return PartialView("");
        }

        public ActionResult Create()
        {
            var categorias = db.Categorias.Where(x => x.IdEstado == 1);
            var subcategorias = db.SubCategorias.Where(x => x.IdEstado == 1);

            SidebarViewModels CatViewModel = new SidebarViewModels();
            CatViewModel.Categorias = categorias.ToList();
            CatViewModel.Subcategorias = subcategorias.ToList();
            return View(CatViewModel);
        }

        public ActionResult Create_2(string categoria)
        {
            AspNetUsers usuario = new AspNetUsers();
            usuario = db.AspNetUsers.Where(x => x.UserName == User.Identity.Name).First();
            PautasPublicitarias pauta = db.PautasPublicitarias.Where(x => x.IdUsuario == usuario.Id).FirstOrDefault();
            ContactViewModels model = new ContactViewModels();
            model.Apellido = usuario.Apellido;
            model.Celular = usuario.PhoneNumber;
            model.Domicilio = usuario.Direccion;
            model.Email = usuario.Email;
            model.Localidad = usuario.Localidad;
            model.Nombre = usuario.Nombre;
            model.Telefono = usuario.TelefonoFijo;
            //CONTROLAR EL TIEMPO DE LA PAUTA
            model.PautaPublicitaria = pauta;
            DateTime hoy = new DateTime();
            hoy = DateTime.Now;
            if (pauta != null)
            {
                //Busco los pagos de esa pauta
                PagosPautas pagoPauta = db.PagosPautas.Where(x => x.IdPauta == pauta.Id).FirstOrDefault();
                int diferenciaDias = (hoy.Date - Convert.ToDateTime(pagoPauta.FechaUltimoPago).Date).Days;
                if (diferenciaDias > 31) //Suponiendo que la pauta es por mes
                {
                    model.PuedePublicar = false;
                }
                else
                {
                    model.PuedePublicar = true;
                }
            }
            else //No tiene pauta publicitaria, vemos si uso sus 2 publicaciones (si es una categoria escencial)
            {
                if (categoria == StaticCategorias.Automotores || categoria == StaticCategorias.Agro || categoria == StaticCategorias.Inmuebles || categoria == StaticCategorias.Electronica)
                {
                    int idCategoria = db.Categorias.Where(x => x.Descripcion == categoria).FirstOrDefault().Id;
                    List<Posts> postsHechos = db.Posts.Where(x => x.IdUsuario == usuario.Id && x.IdCategoria == idCategoria).ToList();
                    int cantEnUnMes = postsHechos.Where(x => (hoy.Date - Convert.ToDateTime(x.FechaPublicacion).Date).Days <= 31).Count();
                    if (cantEnUnMes > 2) //Compruebo que NO tenga 2 en un mes
                    {
                        model.PuedePublicar = false;
                    }
                    else
                    {
                        model.PuedePublicar = true;
                    }
                }
                else
                {
                    model.PuedePublicar = true;
                }
            }
            return View(model);
        }

        public ActionResult HighLight(int id)
        {
            DestacarAvisoViewModels model = new DestacarAvisoViewModels();
            Posts post = db.Posts.Where(x => x.Id == id).FirstOrDefault();
            Imagenes img = db.Imagenes.Where(x => x.IdPost == id).FirstOrDefault();
            if (img != null)
            {
                model.Imagen = img.imagen;
                ImagenDestacada = img.imagen; //Guardo la imagen para mostrarla cuando el destacado fue exitoso
            }          
            model.Titulo = post.Titulo;
            TituloAviso = post.Titulo; //Guardo el titulo para mostrarlo cuando el destacado fue exitoso
            model.IdUsuario = post.IdUsuario;
            model.estaDestacado = post.Prioridad != 1 ? true : false;
            model.IdPost = id;
            return View(model);
        }

        public ActionResult HighLightCorrecto(string collection_id, string collection_status, string external_reference, string payment_type, string merchant_order_id, string preference_id, string site_id, string processing_mode, string merchant_account_id) //PASAR DATOS DE LA RESPONSE DE MERCADOPAGO OK.TXT
        {
            try
            {
                string tipo_destacado_mp = external_reference.Split('-')[1];
                MercadoPagoDestacadoViewModels model = new MercadoPagoDestacadoViewModels
                {
                    collection_id = collection_id,
                    collection_status = collection_status,
                    payment_type = payment_type,
                    merchant_account_id = merchant_account_id,
                    merchant_order_id = merchant_order_id,
                    preference_id = preference_id,
                    processing_mode = processing_mode,
                    site_id = site_id,
                    imagen_destacado = ImagenDestacada,              
                    tipo_destacado = tipo_destacado_mp,
                    titulo = TituloAviso
                };
                return View(model);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult DestacarAviso(GuardarDestacadoViewModels data) //Guardo el destacamiento
        {
            try
            {
                ImagenDestacada = null;
                Posts post = db.Posts.Where(x => x.Id == data.IdPost).FirstOrDefault();
                post.Prioridad = data.Prioridad;
                post.FechaDestacadoDesde = data.FechaDestacadoDesde;
                post.FechaDestacadoHasta = data.FechaDestacadoDesde.Value.AddDays(data.Dias);
                db.Entry(post).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                //Lo guardo en otra tabla para llevar un control de fraude de avisos destacados
                
                return Json(new { });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult Destacar() //Destacar un aviso
        {
            MercadoPago.SDK.AccessToken = "TEST-8433756365588197-021812-a41399140ac01a0b6d7b2e6147ec7940-163942691";
            Preference preference = new Preference();
            preference.Items.Add(new MercadoPago.DataStructures.Preference.Item
            {
                Title = "Producto de prueba",
                Quantity = 1,
                CurrencyId = MercadoPago.Common.CurrencyId.ARS,
                UnitPrice = Convert.ToInt32(150)
            });
            preference.Save();
            return View();
        }

        public ActionResult Create_Image(bool? Preview) //Pantalla aparte para que el usuario suba sus imagenes
        {
            if (Preview == null)
            {
                DeleteImagesFromServer(true, null);
                listaImagenesPosts.Clear();
            }
            return View();
        }

        [HttpPost]
        public ActionResult GetData(PostModels data) //Recibo los datos del Create_3 y guardo el POST
        {
            try
            {
                if (String.IsNullOrEmpty(data.Titulo))
                {
                    throw new Exception();
                }
                string portada_post = null;
                for (int i = 0; i < listaImagenesPosts.Count; i++)
                {
                    if (data.Portada == listaImagenesPosts[i].Nombre)
                    {
                        portada_post = data.Portada;
                    }
                }
                DeleteImagesFromServer(true, null);
                //Borro la imagen optimizada del servidor
                if (System.IO.File.Exists(pathOpt) == true)
                {
                    System.IO.File.Delete(pathOpt);
                }
                //Valido que se haya subido alguna imagen, sino le coloco una predeterminada de ONIX
                //if (listaImagenesPosts.Count() == 0)
                //{

                //}
                // guardar en la db y validar si se guardo correctamente.Redirigir a Index si esta ok.
                AspNetUsers usuario = new AspNetUsers();
                usuario = db.AspNetUsers.Where(x => x.UserName == User.Identity.Name).First();


                //Guardo la info restante del post.
                Posts post = new Posts
                {
                    Titulo = data.Titulo,
                    Precio = data.Precio,
                    Moneda = data.Moneda,
                    Localidad = data.Localidad,
                    Descripcion = data.Descripcion,
                    PermiteContraofertas = data.PermiteContraofertas,
                    PermiteCuotas = data.PermiteCuotas,
                    PermiteCanjes = data.PermiteCanjes,
                    PermitePreguntas = data.PermitePreguntas,
                    Visitas = 0,
                    FechaPublicacion = DateTime.Now,
                    IdUsuario = usuario.Id,
                    IdEstado = 1,
                    IdCategoria = db.Categorias.Where(x => x.Descripcion == data.Categoria).First().Id,
                    IdSubCategoria = db.SubCategorias.Where(x => x.Descripcion == data.SubCategoria).First().Id,
                    NombreUsuario = data.Nombre,
                    ApellidoUsuario = data.Apellido,
                    Email = data.Email,
                    Celular = data.Celular,
                    TelefonoFijo = data.Telefono,
                    Direccion = data.Domicilio,
                    LocalidadContacto = data.LocalidadContacto,
                    IP = Request.UserHostAddress,
                    ApellidoUsuarioVisible = data.ApellidoVisible,
                    EmailVisible = data.EmailVisible,
                    CelularVisible = data.CelularVisible,
                    TelefonoFijoVisible = data.TelefonoFijoVisible,
                    DireccionVisible = data.DomicilioVisible,
                    LocalidadContactoVisible = data.LocalidadContactoVisible,
                    Prioridad = 1,                  
                };
                db.Posts.Add(post);
                db.SaveChanges();
                int idGenerado = post.Id;
                //Guardo las imagenes del post.
                int identifier = 1;
                for (int i = 0; i < listaImagenesPosts.Count; i++)
                {
                    Imagenes image = new Imagenes();
                    image.IdUsuario = usuario.Id;
                    image.nombre = listaImagenesPosts[i].Nombre;
                    image.imagen = listaImagenesPosts[i].Imagen;
                    image.IdPost = idGenerado;
                    if (portada_post == image.nombre)
                    {
                        image.portada = true;
                    }
                    else
                    {
                        image.portada = false;
                    }
                    identifier += 1;
                    db.Imagenes.Add(image);
                }
                db.SaveChanges();

                //Guardo caracteristicas del post
                string[][] caracteristicas = data.Caracteristicas; //EJ: [["marca", "Ford"] , ["modelo", "Escort"]]

                switch (data.Categoria.Trim().ToUpper())
                {
                    case "AGRO":
                        CaracteristicasAgro agro = new CaracteristicasAgro();
                        for (int i = 0; i < caracteristicas.Length; i++)
                        {
                            switch (caracteristicas[i][0])
                            {
                                case "sector":
                                    agro.Sector = caracteristicas[i][1];
                                    break;
                                case "condicion":
                                    agro.Condicion = caracteristicas[i][1];
                                    break;
                                default:
                                    break;
                            }
                        }
                        agro.IdPost = idGenerado;
                        db.CaracteristicasAgro.Add(agro);
                        break;
                    case "AUTOMOTORES":
                        CaracteristicasAutomotores automotores = new CaracteristicasAutomotores();
                        for (int i = 0; i < caracteristicas.Length; i++)
                        {
                            switch (caracteristicas[i][0])
                            {
                                case "km":
                                    automotores.Kilometros = caracteristicas[i][1] == "" ? 0 : Convert.ToInt32(caracteristicas[i][1]);
                                    break;
                                case "marca":
                                    automotores.Marca = caracteristicas[i][1];
                                    break;
                                case "modelo":
                                    automotores.Modelo = caracteristicas[i][1];
                                    break;
                                case "tipovendedor":
                                    automotores.TipoVendedor = caracteristicas[i][1];
                                    break;
                                case "condicion":
                                    automotores.Condicion = caracteristicas[i][1];
                                    break;
                                case "cilindrada":
                                    automotores.Cilindrada = caracteristicas[i][1] == "" ? 0 : Convert.ToDouble(caracteristicas[i][1]);
                                    break;
                                case "combustible":
                                    automotores.Combustible = caracteristicas[i][1];
                                    break;
                                case "ano":
                                    automotores.Ano = caracteristicas[i][1] == "" ? 0 : Convert.ToInt32(caracteristicas[i][1]);
                                    break;
                                default:
                                    break;
                            }
                        }
                        automotores.IdPost = idGenerado;
                        db.CaracteristicasAutomotores.Add(automotores);
                        break;
                    case "ANIMALES":
                        CaracteristicasAnimales animales = new CaracteristicasAnimales();
                        for (int i = 0; i < caracteristicas.Length; i++)
                        {
                            switch (caracteristicas[i][0])
                            {
                                case "tipo":
                                    animales.Tipo = caracteristicas[i][1];
                                    break;
                                default:
                                    break;
                            }
                        }
                        animales.IdPost = idGenerado;
                        db.CaracteristicasAnimales.Add(animales);
                        break;
                    case "BEBES Y NIÑOS":
                        CaracteristicasBebesNinos bebesninos = new CaracteristicasBebesNinos();
                        for (int i = 0; i < caracteristicas.Length; i++)
                        {
                            switch (caracteristicas[i][0])
                            {
                                case "edad":
                                    bebesninos.Edad = Convert.ToInt32(caracteristicas[i][1]);
                                    break;
                                case "condicion":
                                    bebesninos.Condicion = caracteristicas[i][1];
                                    break;
                                default:
                                    break;
                            }
                        }
                        bebesninos.IdPost = idGenerado;
                        db.CaracteristicasBebesNinos.Add(bebesninos);
                        break;
                    case "ELECTRÓNICA":
                        CaracteristicasElectronica electronica = new CaracteristicasElectronica();
                        for (int i = 0; i < caracteristicas.Length; i++)
                        {
                            switch (caracteristicas[i][0])
                            {
                                case "condicion":
                                    electronica.Condicion = caracteristicas[i][1];
                                    break;
                                default:
                                    break;
                            }
                        }
                        electronica.IdPost = idGenerado;
                        db.CaracteristicasElectronica.Add(electronica);
                        break;
                    case "HOGAR Y MUEBLES":
                        CaracteristicasHogarMuebles hogarmuebles = new CaracteristicasHogarMuebles();
                        for (int i = 0; i < caracteristicas.Length; i++)
                        {
                            switch (caracteristicas[i][0])
                            {
                                case "ambiente":
                                    hogarmuebles.Ambiente = caracteristicas[i][1];
                                    break;
                                case "condicion":
                                    hogarmuebles.Condicion = caracteristicas[i][1];
                                    break;
                                default:
                                    break;
                            }
                        }
                        hogarmuebles.IdPost = idGenerado;
                        db.CaracteristicasHogarMuebles.Add(hogarmuebles);
                        break;
                    case "INMUEBLES":
                        CaracteristicasInmuebles inmuebles = new CaracteristicasInmuebles();
                        for (int i = 0; i < caracteristicas.Length; i++)
                        {
                            switch (caracteristicas[i][0])
                            {
                                case "domicilio":
                                    inmuebles.Domicilio = caracteristicas[i][1];
                                    break;
                                case "habitaciones":
                                    inmuebles.Habitaciones = caracteristicas[i][1] == "" ? 0 : Convert.ToInt32(caracteristicas[i][1]);
                                    break;
                                case "superficie":
                                    inmuebles.Superficie = caracteristicas[i][1] == "" ? 0 : Convert.ToInt32(caracteristicas[i][1]);
                                    break;
                                case "banos":
                                    inmuebles.Banos = caracteristicas[i][1] == "" ? 0 : Convert.ToInt32(caracteristicas[i][1]);
                                    break;
                                case "operacion":
                                    inmuebles.Operacion = caracteristicas[i][1];
                                    break;
                                case "tipovendedor":
                                    inmuebles.TipoVendedor = caracteristicas[i][1];
                                    break;
                                case "condicion":
                                    inmuebles.Condicion = caracteristicas[i][1];
                                    break;
                                case "patio":
                                    inmuebles.Patio = caracteristicas[i][1] == "" ? false : Convert.ToBoolean(caracteristicas[i][1]);
                                    break;
                                case "cochera":
                                    inmuebles.Cochera = caracteristicas[i][1] == "" ? false : Convert.ToBoolean(caracteristicas[i][1]);
                                    break;
                                case "cloacas":
                                    inmuebles.Cloacas = Convert.ToBoolean(caracteristicas[i][1]);
                                    break;
                                case "gasnatural":
                                    inmuebles.GasNatural = Convert.ToBoolean(caracteristicas[i][1]);
                                    break;
                                case "aguacaliente":
                                    inmuebles.AguaCaliente = Convert.ToBoolean(caracteristicas[i][1]);
                                    break;
                                default:
                                    break;
                            }
                        }
                        inmuebles.IdPost = idGenerado;
                        db.CaracteristicasInmuebles.Add(inmuebles);
                        break;
                    case "EMPLEOS":
                        CaracteristicasEmpleos empleos = new CaracteristicasEmpleos();
                        for (int i = 0; i < caracteristicas.Length; i++)
                        {
                            switch (caracteristicas[i][0])
                            {
                                case "experiencia":
                                    empleos.ExperienciaMin = Convert.ToInt32(caracteristicas[i][1]);
                                    break;
                                case "rubro":
                                    empleos.Rubro = caracteristicas[i][1];
                                    break;
                                case "salariomin":
                                    if (caracteristicas[i][1] != null && caracteristicas[i][1] != "")
                                    {
                                        empleos.SalarioMin = Convert.ToInt32(caracteristicas[i][1]);
                                    }
                                    else
                                    {
                                        empleos.SalarioMin = null;
                                    }
                                    break;
                                case "salariomax":
                                    if (caracteristicas[i][1] != null && caracteristicas[i][1] != "")
                                    {
                                        empleos.SalarioMax = Convert.ToInt32(caracteristicas[i][1]);
                                    }
                                    else
                                    {
                                        empleos.SalarioMax = null;
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                        empleos.IdPost = idGenerado;
                        db.CaracteristicasEmpleos.Add(empleos);
                        break;
                    case "VARIOS":
                        CaracteristicasVarios varios = new CaracteristicasVarios();
                        for (int i = 0; i < caracteristicas.Length; i++)
                        {
                            switch (caracteristicas[i][0])
                            {
                                case "condicion":
                                    varios.Condicion = caracteristicas[i][1];
                                    break;
                                default:
                                    break;
                            }
                        }
                        varios.IdPost = idGenerado;
                        db.CaracteristicasVarios.Add(varios);
                        break;
                    default:
                        break;
                }
                db.SaveChanges();
                return Json(new { redirectToUrl = Url.Action("Index", "Post", new { id = idGenerado }) });
            }
            catch (Exception e)
            {
                return Json(new { msg = e.Message});
                throw e;
            }
        }

        public ActionResult AgregarFavorito(int idPost) // Agrega post seleccionado a favoritos
        {
            try
            {
                //Checkeo si el post esta ya como favorito para ese usuario
                string message = "";
                string usuActual = db.AspNetUsers.Where(x => x.UserName == User.Identity.Name).FirstOrDefault().Id;
                PostsFavoritos favActual = db.PostsFavoritos.Where(x => x.IdUsuario == usuActual && x.IdPost == idPost).FirstOrDefault();
                if (favActual == null)
                {
                    db.PostsFavoritos.Add(new PostsFavoritos
                    {
                        IdUsuario = usuActual,
                        IdPost = idPost
                    });
                    db.SaveChanges();
                }
                else
                {
                    message = "¡Ya tienes esta publicacion como favorita!";
                }
                return Json(new { message });
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult Favoritos() //Posts favoritos
        {
            string usuActual = db.AspNetUsers.Where(x => x.UserName == User.Identity.Name).FirstOrDefault().Id;
            List<PostExtendedViewModels> model = new List<PostExtendedViewModels>();
            List<PostsFavoritos> ListaFavoritos = db.PostsFavoritos.Where(x => x.IdUsuario == usuActual).ToList();
            foreach (var item in ListaFavoritos)
            {
                Posts post = db.Posts.Where(x => x.Id == item.IdPost).FirstOrDefault();
                List<Imagenes> imagenes = db.Imagenes.Where(x => x.IdPost == post.Id).ToList();
                bool tieneImagen = false;
                foreach (var img in imagenes)
                {
                    if (img.IdPost == post.Id)
                    {
                        tieneImagen = true;
                        model.Add(new PostExtendedViewModels
                        {
                            Descripcion = post.Descripcion,
                            Fecha = post.FechaPublicacion,
                            IdCategoria = post.IdCategoria,
                            IdPost = post.Id,
                            Imagen = img.imagen,
                            Moneda = post.Moneda,
                            PermiteCanjes = post.PermiteCanjes,
                            PermiteContraOfertas = post.PermiteCanjes,
                            PermiteCuotas = post.PermiteCuotas,
                            Precio = post.Precio,
                            Prioridad = post.Prioridad,
                            Titulo = post.Titulo
                        });
                        break;
                    }
                }
                if (!tieneImagen)
                {
                    model.Add(new PostExtendedViewModels
                    {
                        Descripcion = post.Descripcion,
                        Fecha = post.FechaPublicacion,
                        IdCategoria = post.IdCategoria,
                        IdPost = post.Id,
                        Imagen = null,
                        Moneda = post.Moneda,
                        PermiteCanjes = post.PermiteCanjes,
                        PermiteContraOfertas = post.PermiteCanjes,
                        PermiteCuotas = post.PermiteCuotas,
                        Precio = post.Precio,
                        Prioridad = post.Prioridad,
                        Titulo = post.Titulo
                    });
                }

            }
            model = model.OrderByDescending(x => x.Prioridad).ThenByDescending(x => x.Fecha).ToList();
            return View(model);
        }

        public ActionResult Create_3()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GuardarEdicionPost(GuardarEdicionPostViewModels data)
        {
            try
            {
                Posts post = db.Posts.Where(x => x.Id == data.IdPost).FirstOrDefault();
                post.Titulo = data.Titulo;
                post.Precio = data.Precio;
                post.Moneda = data.Moneda;
                post.Localidad = data.Localidad;
                post.Descripcion = data.Descripcion;
                post.NombreUsuario = data.NombreUsuario;
                post.ApellidoUsuario = data.ApellidoUsuario;
                post.Email = data.Email;
                post.Celular = data.Celular;
                post.TelefonoFijo = data.TelefonoFijo;
                post.Direccion = data.Direccion;
                post.LocalidadContacto = data.LocalidadContacto;
                post.PermiteCanjes = data.PermiteCanjes;
                post.PermiteCuotas = data.PermiteCuotas;
                post.PermiteContraofertas = data.PermiteContraofertas;
                post.PermitePreguntas = data.PermitePreguntas;
                post.ApellidoUsuarioVisible = data.ApellidoVisible;
                post.EmailVisible = data.EmailVisible;
                post.CelularVisible = data.CelularVisible;
                post.TelefonoFijoVisible = data.TelefonoVisible;
                post.DireccionVisible = data.DomicilioVisible;
                post.LocalidadContactoVisible = data.LocalidadVisible;
                db.Entry(post).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                string[][] caracteristicasPost = data.CaracteristicasPost;
                switch (data.Categoria.Trim().ToUpper())
                {
                    case "AGRO":
                        CaracteristicasAgro agro = db.CaracteristicasAgro.Where(x => x.IdPost == data.IdPost).FirstOrDefault();
                        for (int i = 0; i < caracteristicasPost.Length; i++)
                        {
                            switch (data.CaracteristicasPost[i][0])
                            {
                                case "sector":
                                    agro.Sector = caracteristicasPost[i][1];
                                    break;
                                case "condicion":
                                    agro.Condicion = caracteristicasPost[i][1];
                                    break;
                                default:
                                    break;
                            }
                        }
                        agro.IdPost = data.IdPost;
                        db.Entry(agro).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        break;
                    case "AUTOMOTORES":
                        CaracteristicasAutomotores automotores = db.CaracteristicasAutomotores.Where(x => x.IdPost == data.IdPost).FirstOrDefault();
                        for (int i = 0; i < caracteristicasPost.Length; i++)
                        {
                            switch (caracteristicasPost[i][0])
                            {
                                case "km":
                                    automotores.Kilometros = Convert.ToInt32(caracteristicasPost[i][1]);
                                    break;
                                case "marca":
                                    automotores.Marca = caracteristicasPost[i][1];
                                    break;
                                case "modelo":
                                    automotores.Modelo = caracteristicasPost[i][1];
                                    break;
                                case "tipovendedor":
                                    automotores.TipoVendedor = caracteristicasPost[i][1];
                                    break;
                                case "condicion":
                                    automotores.Condicion = caracteristicasPost[i][1];
                                    break;
                                case "cilindrada":
                                    automotores.Cilindrada = Convert.ToDouble(caracteristicasPost[i][1]);
                                    break;
                                case "combustible":
                                    automotores.Combustible = caracteristicasPost[i][1];
                                    break;
                                case "ano":
                                    automotores.Ano = Convert.ToInt32(caracteristicasPost[i][1]);
                                    break;
                                default:
                                    break;
                            }
                        }
                        automotores.IdPost = data.IdPost;
                        db.Entry(automotores).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        break;
                    case "ANIMALES":
                        CaracteristicasAnimales animales = db.CaracteristicasAnimales.Where(x => x.IdPost == data.IdPost).FirstOrDefault();
                        for (int i = 0; i < caracteristicasPost.Length; i++)
                        {
                            switch (caracteristicasPost[i][0])
                            {
                                case "tipo":
                                    animales.Tipo = caracteristicasPost[i][1];
                                    break;
                                default:
                                    break;
                            }
                        }
                        animales.IdPost = data.IdPost;
                        db.Entry(animales).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        break;
                    case "BEBES Y NIÑOS":
                        CaracteristicasBebesNinos bebesninos = db.CaracteristicasBebesNinos.Where(x => x.IdPost == data.IdPost).FirstOrDefault();
                        for (int i = 0; i < caracteristicasPost.Length; i++)
                        {
                            switch (caracteristicasPost[i][0])
                            {
                                case "edad":
                                    bebesninos.Edad = Convert.ToInt32(caracteristicasPost[i][1]);
                                    break;
                                case "condicion":
                                    bebesninos.Condicion = caracteristicasPost[i][1];
                                    break;
                                default:
                                    break;
                            }
                        }
                        bebesninos.IdPost = data.IdPost;
                        db.Entry(bebesninos).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        break;
                    case "ELECTRÓNICA":
                        CaracteristicasElectronica electronica = db.CaracteristicasElectronica.Where(x => x.IdPost == data.IdPost).FirstOrDefault();
                        for (int i = 0; i < caracteristicasPost.Length; i++)
                        {
                            switch (caracteristicasPost[i][0])
                            {
                                case "condicion":
                                    electronica.Condicion = caracteristicasPost[i][1];
                                    break;
                                default:
                                    break;
                            }
                        }
                        electronica.IdPost = data.IdPost;
                        db.Entry(electronica).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        break;
                    case "HOGAR Y MUEBLES":
                        CaracteristicasHogarMuebles hogarmuebles = db.CaracteristicasHogarMuebles.Where(x => x.IdPost == data.IdPost).FirstOrDefault();
                        for (int i = 0; i < caracteristicasPost.Length; i++)
                        {
                            switch (caracteristicasPost[i][0])
                            {
                                case "ambiente":
                                    hogarmuebles.Ambiente = caracteristicasPost[i][1];
                                    break;
                                case "condicion":
                                    hogarmuebles.Condicion = caracteristicasPost[i][1];
                                    break;
                                default:
                                    break;
                            }
                        }
                        hogarmuebles.IdPost = data.IdPost;
                        db.Entry(hogarmuebles).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        break;
                    case "INMUEBLES":
                        CaracteristicasInmuebles inmuebles = db.CaracteristicasInmuebles.Where(x => x.IdPost == data.IdPost).FirstOrDefault();
                        for (int i = 0; i < caracteristicasPost.Length; i++)
                        {
                            switch (caracteristicasPost[i][0])
                            {
                                case "domicilio":
                                    inmuebles.Domicilio = caracteristicasPost[i][1];
                                    break;
                                case "habitaciones":
                                    inmuebles.Habitaciones = Convert.ToInt32(caracteristicasPost[i][1]);
                                    break;
                                case "superficie":
                                    inmuebles.Superficie = Convert.ToInt32(caracteristicasPost[i][1]);
                                    break;
                                case "banos":
                                    inmuebles.Banos = Convert.ToInt32(caracteristicasPost[i][1]);
                                    break;
                                case "operacion":
                                    inmuebles.Operacion = caracteristicasPost[i][1];
                                    break;
                                case "tipovendedor":
                                    inmuebles.TipoVendedor = caracteristicasPost[i][1];
                                    break;
                                case "condicion":
                                    inmuebles.Condicion = caracteristicasPost[i][1];
                                    break;
                                case "patio":
                                    inmuebles.Patio = Convert.ToBoolean(caracteristicasPost[i][1]);
                                    break;
                                case "cochera":
                                    inmuebles.Cochera = Convert.ToBoolean(caracteristicasPost[i][1]);
                                    break;
                                case "cloacas":
                                    inmuebles.Cloacas = Convert.ToBoolean(caracteristicasPost[i][1]);
                                    break;
                                case "gasnatural":
                                    inmuebles.GasNatural = Convert.ToBoolean(caracteristicasPost[i][1]);
                                    break;
                                case "aguacaliente":
                                    inmuebles.AguaCaliente = Convert.ToBoolean(caracteristicasPost[i][1]);
                                    break;
                                default:
                                    break;
                            }
                        }
                        inmuebles.IdPost = data.IdPost;
                        db.Entry(inmuebles).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        break;
                    case "EMPLEOS":
                        CaracteristicasEmpleos empleos = db.CaracteristicasEmpleos.Where(x => x.IdPost == data.IdPost).FirstOrDefault();
                        for (int i = 0; i < caracteristicasPost.Length; i++)
                        {
                            switch (caracteristicasPost[i][0])
                            {
                                case "experiencia":
                                    empleos.ExperienciaMin = Convert.ToInt32(caracteristicasPost[i][1]);
                                    break;
                                case "rubro":
                                    empleos.Rubro = caracteristicasPost[i][1];
                                    break;
                                case "salariomin":
                                    if (caracteristicasPost[i][1] != null && caracteristicasPost[i][1] != "")
                                    {
                                        empleos.SalarioMin = Convert.ToInt32(caracteristicasPost[i][1]);
                                    }
                                    else
                                    {
                                        empleos.SalarioMin = null;
                                    }
                                    break;
                                case "salariomax":
                                    if (caracteristicasPost[i][1] != null && caracteristicasPost[i][1] != "")
                                    {
                                        empleos.SalarioMax = Convert.ToInt32(caracteristicasPost[i][1]);
                                    }
                                    else
                                    {
                                        empleos.SalarioMax = null;
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                        empleos.IdPost = data.IdPost;
                        db.Entry(empleos).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        break;
                    case "VARIOS":
                        CaracteristicasVarios varios = db.CaracteristicasVarios.Where(x => x.IdPost == data.IdPost).FirstOrDefault();
                        for (int i = 0; i < caracteristicasPost.Length; i++)
                        {
                            switch (caracteristicasPost[i][0])
                            {
                                case "condicion":
                                    varios.Condicion = caracteristicasPost[i][1];
                                    break;
                                default:
                                    break;
                            }
                        }
                        varios.IdPost = data.IdPost;
                        db.Entry(varios).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        break;
                    default:
                        break;
                }
                db.SaveChanges();
                return Json(new { redirectToUrl = Url.Action("Index", "Post", new { id = data.IdPost }) });
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult EditarPost(int id)
        {
            try
            {
                Posts post = db.Posts.Where(x => x.Id == id).FirstOrDefault();
                ModificarPostViewModels model = new ModificarPostViewModels();
                model.ApellidoUsuario = post.ApellidoUsuario;
                model.ApellidoVisible = post.ApellidoUsuarioVisible;
                model.Categoria = db.Categorias.Where(x => x.Id == post.IdCategoria).FirstOrDefault().Descripcion;
                model.Celular = post.Celular;
                model.CelularVisible = post.CelularVisible;
                model.Descripcion = post.Descripcion;
                model.Direccion = post.Direccion;
                model.DomicilioVisible = post.DireccionVisible;
                model.Email = post.Email;
                model.EmailVisible = post.EmailVisible;
                model.Fecha = post.FechaPublicacion;
                model.IdPost = post.Id;
                model.IdUsuario = post.IdUsuario;
                model.ListaImagenes = db.Imagenes.Where(x => x.IdPost == post.Id).ToList();
                model.Localidad = post.Localidad;
                model.LocalidadContacto = post.LocalidadContacto;
                model.LocalidadVisible = post.LocalidadContactoVisible;
                model.Moneda = post.Moneda;
                model.NombreUsuario = post.NombreUsuario;
                model.PermiteCanjes = post.PermiteCanjes;
                model.PermiteContraofertas = post.PermiteContraofertas;
                model.PermiteCuotas = post.PermiteCuotas;
                model.PermitePreguntas = post.PermitePreguntas;
                model.Precio = post.Precio;
                model.SubCategoria = db.SubCategorias.Where(x => x.Id == post.IdSubCategoria).FirstOrDefault().Descripcion;
                model.TelefonoFijo = post.TelefonoFijo;
                model.TelefonoVisible = post.TelefonoFijoVisible;
                model.Titulo = post.Titulo;
                model.Visitas = post.Visitas;
                switch (post.IdCategoria)
                {
                    case 1:
                        model.Automotores = db.CaracteristicasAutomotores.Where(x => x.IdPost == post.Id).FirstOrDefault();
                        break;
                    case 2:
                        model.Agro = db.CaracteristicasAgro.Where(x => x.IdPost == post.Id).FirstOrDefault();
                        break;
                    case 3:
                        model.Animales = db.CaracteristicasAnimales.Where(x => x.IdPost == post.Id).FirstOrDefault();
                        break;
                    case 4:
                        model.BebesNinos = db.CaracteristicasBebesNinos.Where(x => x.IdPost == post.Id).FirstOrDefault();
                        break;
                    case 5:
                        model.Electronica = db.CaracteristicasElectronica.Where(x => x.IdPost == post.Id).FirstOrDefault();
                        break;
                    case 6:
                        model.Empleos = db.CaracteristicasEmpleos.Where(x => x.IdPost == post.Id).FirstOrDefault();
                        break;
                    case 7:
                        model.HogarMuebles = db.CaracteristicasHogarMuebles.Where(x => x.IdPost == post.Id).FirstOrDefault();
                        break;
                    case 8:
                        model.Inmuebles = db.CaracteristicasInmuebles.Where(x => x.IdPost == post.Id).FirstOrDefault();
                        break;
                    case 9:
                        model.Varios = db.CaracteristicasVarios.Where(x => x.IdPost == post.Id).FirstOrDefault();
                        break;
                    default:
                        break;
                }
                return View(model);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public ActionResult EliminarPost(EliminarPostViewModels data)
        {
            try
            {
                EliminarPublicacion(data);
                return Json(new { });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void EliminarPublicacion(EliminarPostViewModels data)
        {
            List<Imagenes> ImagenesBorrar = db.Imagenes.Where(x => x.IdPost == data.idPost).ToList();
            db.Imagenes.RemoveRange(ImagenesBorrar);
            db.SaveChanges();
            switch (data.idCategoria)
            {
                case 1:
                    List<CaracteristicasAutomotores> ListaAutomotores = db.CaracteristicasAutomotores.Where(x => x.IdPost == data.idPost).ToList();
                    db.CaracteristicasAutomotores.RemoveRange(ListaAutomotores);
                    break;
                case 2:
                    List<CaracteristicasAgro> ListaAgro = db.CaracteristicasAgro.Where(x => x.IdPost == data.idPost).ToList();
                    db.CaracteristicasAgro.RemoveRange(ListaAgro);
                    break;
                case 3:
                    List<CaracteristicasAnimales> ListaAnimales = db.CaracteristicasAnimales.Where(x => x.IdPost == data.idPost).ToList();
                    db.CaracteristicasAnimales.RemoveRange(ListaAnimales);
                    break;
                case 4:
                    List<CaracteristicasBebesNinos> ListaBebesNinos = db.CaracteristicasBebesNinos.Where(x => x.IdPost == data.idPost).ToList();
                    db.CaracteristicasBebesNinos.RemoveRange(ListaBebesNinos);
                    break;
                case 5:
                    List<CaracteristicasElectronica> ListaElectronica = db.CaracteristicasElectronica.Where(x => x.IdPost == data.idPost).ToList();
                    db.CaracteristicasElectronica.RemoveRange(ListaElectronica);
                    break;
                case 6:
                    List<CaracteristicasEmpleos> ListaEmpleos = db.CaracteristicasEmpleos.Where(x => x.IdPost == data.idPost).ToList();
                    db.CaracteristicasEmpleos.RemoveRange(ListaEmpleos);
                    break;
                case 7:
                    List<CaracteristicasHogarMuebles> ListaMuebles = db.CaracteristicasHogarMuebles.Where(x => x.IdPost == data.idPost).ToList();
                    db.CaracteristicasHogarMuebles.RemoveRange(ListaMuebles);
                    break;
                case 8:
                    List<CaracteristicasInmuebles> ListaInmuebles = db.CaracteristicasInmuebles.Where(x => x.IdPost == data.idPost).ToList();
                    db.CaracteristicasInmuebles.RemoveRange(ListaInmuebles);
                    break;
                case 9:
                    List<CaracteristicasVarios> ListaVarios = db.CaracteristicasVarios.Where(x => x.IdPost == data.idPost).ToList();
                    db.CaracteristicasVarios.RemoveRange(ListaVarios);
                    break;
                default:
                    break;
            }
            db.SaveChanges();
            List<Contraofertas> contraofertasPost = db.Contraofertas.Where(x => x.IdPost == data.idPost).ToList();
            db.Contraofertas.RemoveRange(contraofertasPost);
            db.SaveChanges();
            List<ImagenesCanjes> ImgCanjesPost = db.ImagenesCanjes.Where(x => x.IdPost == data.idPost).ToList();
            db.ImagenesCanjes.RemoveRange(ImgCanjesPost);
            db.SaveChanges();
            List<Canjes> canjesPost = db.Canjes.Where(x => x.IdPost == data.idPost).ToList();
            db.Canjes.RemoveRange(canjesPost);
            db.SaveChanges();
            List<Comentarios> comentariosPost = db.Comentarios.Where(x => x.IdPost == data.idPost).ToList();
            db.Comentarios.RemoveRange(comentariosPost);
            db.SaveChanges();
            List<PostDenunciados> postDenunciados = db.PostDenunciados.Where(x => x.IdPost == data.idPost).ToList();
            db.PostDenunciados.RemoveRange(postDenunciados);
            db.SaveChanges();
            List<PostsFavoritos> postFavs = db.PostsFavoritos.Where(x => x.IdPost == data.idPost).ToList();
            db.PostsFavoritos.RemoveRange(postFavs);
            db.SaveChanges();
            Posts post = db.Posts.Where(x => x.Id == data.idPost).FirstOrDefault();
            db.Posts.Remove(post);
            db.SaveChanges();
        }

        [HttpPost]
        public ActionResult UploadFile(int? IdPost) //BORRAR IMAGENES TEMPORALES DEL SERVIDOR SI SE DESCARTA UN POST O SI SE DESCARTA EL CONTRACANJE
        {
            try
            {
                ViewBag.PostNumber = null;
                bool crea_post = false;
                if (IdPost == null) //Esta creando un post (caso contrario estaria creando un canje)
                {
                    crea_post = true;
                }
                DeleteImagesFromServer(crea_post, null);
                listaImagenesPosts.Clear();
                listaImagenesCanjes.Clear();
                List<string> files_uploaded = new List<string>();
                List<string> files_routes = new List<string>();
                if (Request.Files.Count > 0)
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        var file = Request.Files[i];
                        if (file.ContentLength > 0 && file != null)
                        {
                            if (file.ContentType == "image/jpg" || file.ContentType == "image/jpeg" || file.ContentType == "image/png" || file.ContentType == "image/webp")
                            {
                                byte[] img = ConvertToBytes(file); //Ya viene en blob (bytes)
                                IdImagen = "";

                                //Guardo img en carpeta temporal del serivdor
                                var path = ControllerContext.HttpContext.Server.MapPath("/Content");
                                var filename = Path.Combine(path, Path.GetFileName(file.FileName));

                                List<string> rutasABorrar = new List<string>();
                                rutasABorrar.Add(Path.Combine(path, filename));

                                System.IO.File.WriteAllBytes(Path.Combine(path, filename), img);

                                //Reduce el tamaño de la imagen con una API externa.
                                pathOpt = subirImagenCloudinary(Path.Combine(path, Path.GetFileName(file.FileName)));

                                //Reemplazo la imagen subida al servidor anteriormente por la optimizada
                                if (System.IO.File.Exists(filename) == true)
                                {
                                    System.IO.File.Delete(filename);
                                }

                                //Obtengo imagen optimizada del servidor 
                                //Transformo la imagen en un array de bytes
                                byte[] imgOpt = System.IO.File.ReadAllBytes(pathOpt);

                                //Guardo la imagen optimizada obtenida de cloudinary
                                ImagenViewModels image = new ImagenViewModels();
                                image.Imagen = imgOpt; //img de cloudinary
                                image.Nombre = IdImagen + ".jpg"; //nombre de la imagen de cloudinary
                                int tamano = imgOpt.Length;
                                if (IdPost != null) //Si es un canje
                                {
                                    listaImagenesCanjes.Add(image);
                                }
                                else
                                {
                                    listaImagenesPosts.Add(image);
                                }

                                TempData["UploadSuccess"] = true;
                                files_uploaded.Add(image.Nombre);
                                files_routes.Add(image.Nombre);
                            }
                        }
                    }
                    TempData["FilesNames"] = files_uploaded;
                    TempData["FilesRoute"] = files_routes;
                }
                return Json(new { });
            }
            catch (Exception e)
            {
                TempData["UploadSuccess"] = false;
                throw e;
            }

            //if (IdPost != null) //Canjes
            //{
            //    return RedirectToAction("Index", "Post", new { Id = IdPost });
            //}
        }

        public ActionResult PreviewImage(string file)
        {
            var path = ControllerContext.HttpContext.Server.MapPath("/Content");
            if (System.IO.File.Exists(Path.Combine(path, file)))
            {
                return File(Path.Combine(path, file), "image/jpg");
            }
            return new HttpNotFoundResult();
        }

        public ActionResult DecodeImage(byte[] file)
        {
            if (file != null)
            {
                return File(file, "image/jpg");
            }
            else
            {
                return null;
            }
        }

        public byte[] ConvertToBytes(HttpPostedFileBase file)
        {
            byte[] imageBytes = null;
            BinaryReader reader = new BinaryReader(file.InputStream);
            imageBytes = reader.ReadBytes((int)file.ContentLength);

            return imageBytes;
        }

        public void DeleteImagesFromServer(bool crea_post, bool? borra_img_canjes_desde_vista)
        {
            //borro las imagenes guardadas temporalmente
            var pathServer = ControllerContext.HttpContext.Server.MapPath("/Content");

            if (crea_post)
            {
                for (int i = 0; i < listaImagenesPosts.Count; i++)
                {
                    var FileToDelete = pathServer + "\\" + listaImagenesPosts[i].Nombre;
                    if (System.IO.File.Exists(FileToDelete) == true)
                    {
                        System.IO.File.Delete(FileToDelete);
                    }
                }
            }
            else
            {
                for (int i = 0; i < listaImagenesCanjes.Count; i++)
                {
                    var FileToDelete = pathServer + "\\" + listaImagenesCanjes[i].Nombre;
                    if (System.IO.File.Exists(FileToDelete) == true)
                    {
                        System.IO.File.Delete(FileToDelete);
                    }
                }
                if (borra_img_canjes_desde_vista != null)
                {
                    if (borra_img_canjes_desde_vista == true)
                    {
                        listaImagenesCanjes.Clear();
                    }
                }
            }
        }

        public string subirImagenCloudinary(string imagePath)
        {
            try
            {
                Account account = new Account("onix", "988849462435128", "tE9eWvURd2rP6RcEFNUru-4Ucqw");
                Cloudinary cloudinary = new Cloudinary(account);
                var uploadParameters = new ImageUploadParams()
                {
                    File = new FileDescription(imagePath)
                };
                var uploadResult = cloudinary.Upload(uploadParameters);
                IdImagen = uploadResult.PublicId; //ID GENERADO DE LA IMG SUBIDA.
                var imgOptimizada = cloudinary.Api.UrlImgUp.Transform(new Transformation().Height(520).Quality("auto:best").Crop("scale")).BuildUrl(uploadResult.PublicId);
                using (var client = new WebClient())
                {
                    client.DownloadFile(imgOptimizada, ControllerContext.HttpContext.Server.MapPath("/Content") + @"\" + IdImagen + ".jpg");
                }
                return ControllerContext.HttpContext.Server.MapPath("/Content") + @"\" + IdImagen + ".jpg";
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}