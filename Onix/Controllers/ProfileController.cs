﻿using Datos;
using Microsoft.AspNet.Identity;
using Onix.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Onix.Controllers
{
    public class ProfileController : Controller
    {
        private onixDBEntities db = new onixDBEntities();

        // GET: Profile
        public ActionResult Index(string id, bool? instr = false) //Perfil publico
        {
            PerfilUsuarioViewModels model = new PerfilUsuarioViewModels();
            AspNetUsers usuario = new AspNetUsers();
            usuario = db.AspNetUsers.Where(x => x.Id == id).FirstOrDefault();
            ImagenesUsuarios imgUsuarioActual = db.ImagenesUsuarios.Where(x => x.IdUsuario == id).FirstOrDefault();
            if (imgUsuarioActual != null)
            {
                model.Imagen = imgUsuarioActual.imagen;
            }
            else
            {
                model.Imagen = null;
            }
            model.Id = id;
            model.Nombre = usuario.Nombre;
            model.Apellido = usuario.Apellido;
            model.Localidad = usuario.Localidad;
            model.Email = usuario.Email;
            model.BrindaBuenaAtencion = db.AspNetUsers.Where(x => x.Id == id).FirstOrDefault().BrindaBuenaAtencion;
            model.ValidadoConFacebook = db.AspNetUsers.Where(x => x.Id == id).FirstOrDefault().ValidadoConFacebook;
            List<Votos> votosRecibidosUsu = db.Votos.Where(x => x.IdUsuarioDestino == id).ToList();
            if (votosRecibidosUsu.Count() > 0)
            {
                int votosPositivosUsu = votosRecibidosUsu.Where(x => x.VotoPositivo == true).Count();
                decimal valoracionesPositivas = (votosPositivosUsu * 100) / votosRecibidosUsu.Count();
                model.ValoracionesPositivas = Convert.ToInt32(valoracionesPositivas);
            }
            else
            {
                model.ValoracionesPositivas = -1;
            }
            List<Posts> postsUsuario = db.Posts.Where(x => x.IdUsuario == id && x.IdEstado == 1).OrderByDescending(x => x.FechaPublicacion).ToList();
            List<PostExtendedViewModels> listaPosts = new List<PostExtendedViewModels>();
            foreach (var post in postsUsuario)
            {
                Imagenes imgPost = db.Imagenes.Where(x => x.IdPost == post.Id && x.portada == true).FirstOrDefault();
                if (imgPost != null)
                {
                    PostExtendedViewModels modelPost = new PostExtendedViewModels();
                    modelPost.IdPost = post.Id;
                    modelPost.IdCategoria = post.IdCategoria;
                    modelPost.Precio = post.Precio;
                    modelPost.Moneda = post.Moneda;
                    modelPost.Descripcion = post.Descripcion;
                    modelPost.Imagen = imgPost.imagen;
                    modelPost.Titulo = post.Titulo;
                    modelPost.PermiteCanjes = post.PermiteCanjes;
                    modelPost.PermiteContraOfertas = post.PermiteContraofertas;
                    modelPost.PermiteCuotas = post.PermiteCuotas;
                    listaPosts.Add(modelPost);
                }
                else
                {
                    PostExtendedViewModels modelPost = new PostExtendedViewModels();
                    modelPost.IdPost = post.Id;
                    modelPost.IdCategoria = post.IdCategoria;
                    modelPost.Precio = post.Precio;
                    modelPost.Moneda = post.Moneda;
                    modelPost.Descripcion = post.Descripcion;
                    modelPost.Imagen = null;
                    modelPost.Titulo = post.Titulo;
                    modelPost.PermiteCanjes = post.PermiteCanjes;
                    modelPost.PermiteContraOfertas = post.PermiteContraofertas;
                    modelPost.PermiteCuotas = post.PermiteCuotas;
                    modelPost.Prioridad = post.Prioridad;
                    listaPosts.Add(modelPost);
                }
            }
            model.ListaPosts = listaPosts;
            model.ListaPosts = model.ListaPosts.OrderByDescending(x => x.Prioridad).ThenByDescending(x => x.Fecha).ToList();
            model.CantidadPublicaciones = model.ListaPosts.Count();
            ViewBag.Instructivo = instr;
            return View(model);
        }

        public ActionResult Favorites()
        {
            return View();
        }

        public ActionResult Configuration() //Apartado de configuracion personal
        {
            ConfigurationViewModels model = new ConfigurationViewModels();
            AspNetUsers usuario = new AspNetUsers();
            usuario = db.AspNetUsers.Where(x => x.UserName == User.Identity.Name).First();
            ImagenesUsuarios imgUsuarioActual = db.ImagenesUsuarios.Where(x => x.IdUsuario == usuario.Id).FirstOrDefault();
            if (imgUsuarioActual != null)
            {
                model.Imagen = imgUsuarioActual.imagen;
            }
            else
            {
                model.Imagen = null;
            }
            model.Nombre = usuario.Nombre;
            model.Apellido = usuario.Apellido;
            model.Domicilio = usuario.Direccion;
            model.Celular = usuario.PhoneNumber;
            model.Email = usuario.Email;
            model.Localidad = usuario.Localidad;
            model.Telefono = usuario.TelefonoFijo;

            return View(model);
        }

        [HttpPost]
        public ActionResult VotarUsuario(VotarUsuarioViewModel data)
        {
            try
            {
                string idVotante = db.AspNetUsers.Where(x => x.UserName == data.IdUsuarioOrigen).FirstOrDefault().Id;
                Votos votoPrevio = db.Votos.Where(x => x.IdUsuarioOrigen == idVotante && x.IdUsuarioDestino == data.IdUsuarioDestino).FirstOrDefault();
                if (votoPrevio == null)
                {
                    Votos voto = new Votos();
                    voto.Fecha = DateTime.Now;
                    voto.IdUsuarioOrigen = idVotante;
                    voto.IdUsuarioDestino = data.IdUsuarioDestino;
                    voto.VotoPositivo = data.VotoPositivo;
                    db.Votos.Add(voto);
                    db.SaveChanges();
                }
                else
                {
                    votoPrevio.Fecha = DateTime.Now;
                    votoPrevio.IdUsuarioOrigen = idVotante;
                    votoPrevio.IdUsuarioDestino = data.IdUsuarioDestino;
                    votoPrevio.VotoPositivo = data.VotoPositivo;
                    db.Entry(votoPrevio).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
                return Json(new { });
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public ActionResult GuardarDatosPersonales(DatosPersonalesViewModels data)
        {
            bool exito = false;
            AspNetUsers usuario = db.AspNetUsers.Where(x => x.UserName == data.EmailOriginal).FirstOrDefault();
            if (usuario != null)
            {
                usuario.Nombre = data.Nombre;
                usuario.Apellido = data.Apellido;
                usuario.Direccion = data.Domicilio;
                usuario.Email = data.Email;
                usuario.Localidad = data.Localidad;
                usuario.TelefonoFijo = data.Telefono;
                usuario.PhoneNumber = data.Celular;
                db.Entry(usuario).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                exito = true;
            }
            else
            {
                exito = false;
            }
            return Json(new { exito });
        }

        [HttpPost]
        public ActionResult EliminarCuenta(string email)
        {
            try
            {
                bool exito = false;
                AspNetUsers usu = db.AspNetUsers.Where(x => x.UserName == email).FirstOrDefault();
                if (usu != null)
                {
                    List<Contraofertas> contraofertasUsu = db.Contraofertas.Where(x => x.IdUsuario == usu.Id).ToList();
                    db.Contraofertas.RemoveRange(contraofertasUsu);
                    db.SaveChanges();
                    List<ImagenesCanjes> ImgCanjesPost = db.ImagenesCanjes.Where(x => x.IdUsuario == usu.Id).ToList();
                    db.ImagenesCanjes.RemoveRange(ImgCanjesPost);
                    db.SaveChanges();
                    List<Canjes> canjesUsu = db.Canjes.Where(x => x.IdUsuario == usu.Id).ToList();
                    db.Canjes.RemoveRange(canjesUsu);
                    db.SaveChanges();
                    List<Comentarios> comentariosUsu = db.Comentarios.Where(x => x.IdUsuario == usu.Id).ToList();
                    db.Comentarios.RemoveRange(comentariosUsu);
                    db.SaveChanges();
                    List<ComentariosDenunciados> comentariosDenunUsu = db.ComentariosDenunciados.Where(x => x.IdUsuario == usu.Id).ToList();
                    db.ComentariosDenunciados.RemoveRange(comentariosDenunUsu);
                    db.SaveChanges();
                    List<ImagenesUsuarios> ImgUsu = db.ImagenesUsuarios.Where(x => x.IdUsuario == usu.Id).ToList();
                    db.ImagenesUsuarios.RemoveRange(ImgUsu);
                    db.SaveChanges();
                    List<Notificaciones> Notifusu = db.Notificaciones.Where(x => x.IdUsuarioOrigen == usu.Id || x.IdUsuarioDestino == usu.Id).ToList();
                    db.Notificaciones.RemoveRange(Notifusu);
                    db.SaveChanges();
                    List<PostsFavoritos> Favs = db.PostsFavoritos.Where(x => x.IdUsuario == usu.Id).ToList();
                    db.PostsFavoritos.RemoveRange(Favs);
                    db.SaveChanges();
                    List<Votos> votos = db.Votos.Where(x => x.IdUsuarioOrigen == usu.Id || x.IdUsuarioDestino == usu.Id).ToList();
                    db.Votos.RemoveRange(votos);
                    db.SaveChanges();

                    //Llamo a funcion de EliminarPost
                    List<Posts> Posts = db.Posts.Where(x => x.IdUsuario == usu.Id).ToList();
                    PostController postController = new PostController();
                    foreach (var post in Posts)
                    {
                        EliminarPostViewModels eliminarPost = new EliminarPostViewModels();
                        eliminarPost.idPost = post.Id;
                        eliminarPost.idCategoria = post.IdCategoria;
                        postController.EliminarPublicacion(eliminarPost);
                    }
                    db.SaveChanges();
                    usu.IdEstado = 3;
                    db.Entry(usu).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    exito = true;
                }
                return Json(new { exito });
            }
            catch (Exception e)
            {

                throw e;
            }           
        }

        [HttpPost]
        public ActionResult UploadFileProfile()
        {
            ViewBag.UploadSuccess = false;
            try
            {
                if (Request.Files.Count > 0)
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        var file = Request.Files[i];
                        if (file.ContentLength > 0 && file != null)
                        {
                            if (Path.GetExtension(file.FileName).ToLower() == ".jpg" || Path.GetExtension(file.FileName).ToLower() == ".jpeg" || Path.GetExtension(file.FileName).ToLower() == ".png")
                            {
                                //Reducir el tamaño de la imagen con una API externa. (CLOUDINARY)

                                byte[] img = ConvertToBytes(file);
                                ImagenViewModels image = new ImagenViewModels();
                                image.Imagen = img;
                                image.Nombre = file.FileName;

                                //GUARDADO EN LA BD
                                AspNetUsers usuario = new AspNetUsers();
                                usuario = db.AspNetUsers.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
                                ImagenesUsuarios imgUsuarioActual = db.ImagenesUsuarios.Where(x => x.IdUsuario == usuario.Id).FirstOrDefault();
                                if (imgUsuarioActual == null) //El usuario no puso foto aun. La creamos.
                                {
                                    ImagenesUsuarios images = new ImagenesUsuarios
                                    {
                                        IdUsuario = usuario.Id,
                                        imagen = img,
                                        nombre = file.FileName
                                    };
                                    db.ImagenesUsuarios.Add(images);
                                }
                                else //El usuario ya tiene una foto. La actualizamos.
                                {
                                    imgUsuarioActual.imagen = img;
                                    imgUsuarioActual.nombre = file.FileName;
                                    db.Entry(imgUsuarioActual).State = System.Data.Entity.EntityState.Modified;
                                }
                                db.SaveChanges();
                                ViewBag.UploadSuccess = true;
                                ViewBag.Message = "";
                            }
                            else
                            {
                                ViewBag.Message = "Formato de imagen no permitido. Sólo se permiten archivos con extensión .png y .jpg";
                            }
                        }
                    }
                }
                else
                {
                    ViewBag.Message = "Ha ocurrido un error. Por favor, Intentá más tarde nuevamente";
                }
            }
            catch (Exception)
            {
                ViewBag.UploadSuccess = false;
                ViewBag.Message = "Ha ocurrido un error. Por favor, Intentá más tarde nuevamente";
            }
            TempData["UploadSuccess"] = ViewBag.UploadSuccess;
            TempData["Message"] = ViewBag.Message;
            return RedirectToAction("Configuration", "Profile");
        }

        public byte[] ConvertToBytes(HttpPostedFileBase file)
        {
            byte[] imageBytes = null;
            BinaryReader reader = new BinaryReader(file.InputStream);
            imageBytes = reader.ReadBytes((int)file.ContentLength);

            return imageBytes;
        }
    }
}