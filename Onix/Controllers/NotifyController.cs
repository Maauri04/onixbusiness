﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Onix.Controllers
{
    public class NotifyController : Controller
    {
        onixDBEntities db = new onixDBEntities();

        public ActionResult ActualizarNotificacion(int idNotificacionFinal)
        {
            try
            {
                Notificaciones notifyActual = new Notificaciones();
                notifyActual = db.Notificaciones.Where(x => x.Id == idNotificacionFinal).FirstOrDefault();
                notifyActual.Leida = true;
                db.Entry(notifyActual).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return Json(new { });
                
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}