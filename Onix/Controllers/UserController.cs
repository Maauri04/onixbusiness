﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Datos;
using Onix.Models;

namespace Onix.Controllers
{
    public class UserController : Controller
    {
        private onixDBEntities db = new onixDBEntities();

        public ActionResult _Navigation()
        {
            AspNetUsers usuario = new AspNetUsers();
            usuario = db.AspNetUsers.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            
            UserViewModels model = new UserViewModels();
            if (usuario != null)
            {
                model.IdUsuario = usuario.Id;
                if (usuario.Nombre != null)
                {
                    model.Nombre = usuario.Nombre;
                }
                else
                {
                    model.Nombre = "Usuario";
                }
                ImagenesUsuarios imgUsu = usuario.ImagenesUsuarios.FirstOrDefault();
                if (imgUsu != null)
                {
                    model.Imagen = imgUsu.imagen;
                }
                else
                {
                    model.Imagen = null;
                }
                model.Notificaciones = db.Notificaciones.Where(x => x.IdUsuarioDestino == usuario.Id).ToList();
                return PartialView("_Navigation", model);
            } 
            else
            {
                return PartialView("_Navigation");
            }

        }    
    }
}
