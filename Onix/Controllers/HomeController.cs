﻿using Datos;
using Microsoft.AspNet.Identity.Owin;
using Onix.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Onix.Controllers
{
    public class HomeController : Controller
    {
        private onixDBEntities db = new onixDBEntities();

        public ActionResult Index(int pagina = 1) //Si no recibe pagina, es 1
        {
            //RECORRER TODOS LOS POSTS Y POR CADA UNO, BUSCAR SUS IMAGENES ASOCIADAS POR EL ID DE POST.
            HomeViewModels model = new HomeViewModels();
            List<Posts> posts = new List<Posts>();
            //List<Imagenes> imagenes = new List<Imagenes>();
            ActualizarDestacados();
            posts = db.Posts.Where(x => x.IdEstado == 1).OrderByDescending(x => x.Prioridad).ThenByDescending(x => x.FechaPublicacion).Skip((pagina - 1) * Convert.ToInt32(ConfigurationManager.AppSettings["PostsPorPagina"])).Take(Convert.ToInt32(ConfigurationManager.AppSettings["PostsPorPagina"])).ToList();
            model.TotalDeRegistros = db.Posts.Count();
            model.PaginaActual = pagina;
            model.RegistrosPorPagina = Convert.ToInt32(ConfigurationManager.AppSettings["PostsPorPagina"]);
            foreach (var post in posts)
            {
                Imagenes imgPost = db.Imagenes.Where(x => x.IdPost == post.Id && x.portada == true).FirstOrDefault();
                if (imgPost != null)
                {
                    PostHomeViewModels postmodel = new PostHomeViewModels();
                    postmodel.idPost = post.Id;
                    postmodel.Imagen = imgPost.imagen;
                    postmodel.Titulo = post.Titulo;
                    postmodel.Prioridad = post.Prioridad;
                    model.ListaPosts.Add(postmodel);
                }
                else
                {
                    PostHomeViewModels postmodel = new PostHomeViewModels();
                    postmodel.idPost = post.Id;
                    postmodel.Imagen = null;
                    postmodel.Titulo = post.Titulo;
                    postmodel.Prioridad = post.Prioridad;
                    model.ListaPosts.Add(postmodel);
                }
            }

            ViewBag.DatosIncompletos = false;
            AspNetUsers usuario = db.AspNetUsers.Where(x => x.UserName == User.Identity.Name).FirstOrDefault();
            if (usuario != null)
            {
                model.idEstado = usuario.IdEstado;
                if (usuario.Direccion != null && usuario.Localidad != null && usuario.PhoneNumber != null)
                {
                    ViewBag.DatosIncompletos = true;
                }
            }
            model.ListaPosts = model.ListaPosts.OrderByDescending(x => x.Prioridad).ThenByDescending(x => x.FechaPublicacion).ToList();
            TempData["UploadSuccess"] = ViewBag.UploadSuccess;
            return View(model);
        }

        public void ActualizarDestacados() //Controlo el tiempo de vigencia de los posts destacados
        {
            try
            {
                List<Posts> postsDestacados = db.Posts.Where(x => x.Prioridad == 2 || x.Prioridad == 3).ToList();
                List<Posts> postsActualizar = new List<Posts>();
                DateTime hoy = new DateTime();
                hoy = DateTime.Now;
                foreach (var item in postsDestacados)
                {
                    int diferenciaDias = (hoy.Date - Convert.ToDateTime(item.FechaDestacadoHasta).Date).Days;
                    if (diferenciaDias > 0)
                    {
                        //POST DESTACADO CADUCADO
                        postsActualizar.Add(item);
                    }
                }
                postsActualizar.ForEach(x => x.Prioridad = 1);
                db.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> EnviarConsulta(EnviarMailViewModels data)
        {
            try
            {
                string onixUser = db.AspNetUsers.Where(x => x.UserName == "info@onixbusiness.com.ar").FirstOrDefault().Id;
                await UserManager.SendEmailAsync(onixUser, "CONSULTA DE USUARIO", "Nombre: " + data.Nombre + "<br />" + "Email: " + data.Email + "<br />" + "Motivo: " + data.Motivo + "<br />" + "Consulta: " + data.Mensaje);
                return Json(new { });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public ActionResult Politica()
        {
            return View();
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}