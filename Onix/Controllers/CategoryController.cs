﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Datos;
using Onix.Models;

namespace Onix.Controllers
{
    public class CategoryController : Controller
    {
        private onixDBEntities db = new onixDBEntities();

        public ActionResult Index(int idCat, int idSubCat, int pagina = 1) //Falta permite contraofertas, cuotas y canjes
        {
            CategoryViewModels model = new CategoryViewModels();
            string categoria = db.Categorias.Where(x => x.Id == idCat && x.IdEstado == 1).FirstOrDefault().Descripcion;
            string subcategoria = null;
            List<Posts> ListaPosts = new List<Posts>();
            if (idSubCat > -1)
            {
                subcategoria = db.SubCategorias.Where(x => x.Id == idSubCat && x.IdEstado == 1).FirstOrDefault().Descripcion;
                ListaPosts = db.Posts.Where(x => x.IdCategoria == idCat && x.IdSubCategoria == idSubCat).OrderByDescending(x => x.FechaPublicacion).ThenByDescending(x => x.Id).Skip((pagina - 1) * Convert.ToInt32(ConfigurationManager.AppSettings["PostsPorPagina"])).Take(Convert.ToInt32(ConfigurationManager.AppSettings["PostsPorPagina"])).ToList();
            }
            else //Eligio "Todas" en subcategorias
            {
                ListaPosts = db.Posts.Where(x => x.IdCategoria == idCat).OrderByDescending(x => x.FechaPublicacion).ThenByDescending(x => x.Id).Skip((pagina - 1) * Convert.ToInt32(ConfigurationManager.AppSettings["PostsPorPagina"])).Take(Convert.ToInt32(ConfigurationManager.AppSettings["PostsPorPagina"])).ToList();
            }
            
            model.TotalDeRegistros = db.Posts.Count();
            model.PaginaActual = pagina;
            model.RegistrosPorPagina = Convert.ToInt32(ConfigurationManager.AppSettings["PostsPorPagina"]);
            model.Categoria = categoria;
            model.SubCategoria = subcategoria;
            model.IdCategoria = idCat;
            model.IdSubCategoria = idSubCat;

            List<PostExtendedViewModels> listaPosts = new List<PostExtendedViewModels>();
            foreach (var post in ListaPosts)
            {
                Imagenes imgPost = db.Imagenes.Where(x => x.IdPost == post.Id && x.portada == true).FirstOrDefault();
                if (imgPost != null)
                {
                    PostExtendedViewModels modelPost = new PostExtendedViewModels();
                    modelPost.IdPost = post.Id;
                    modelPost.Precio = post.Precio;
                    modelPost.Moneda = post.Moneda;
                    modelPost.Descripcion = post.Descripcion;
                    modelPost.Imagen = imgPost.imagen;
                    modelPost.Titulo = post.Titulo;
                    modelPost.PermiteCanjes = post.PermiteCanjes;
                    modelPost.PermiteContraOfertas = post.PermiteContraofertas;
                    modelPost.PermiteCuotas = post.PermiteCuotas;
                    modelPost.Fecha = post.FechaPublicacion;
                    modelPost.Prioridad = post.Prioridad;
                    modelPost.Localidad = post.Localidad;
                    listaPosts.Add(modelPost);
                }
                else
                {
                    PostExtendedViewModels modelPost = new PostExtendedViewModels();
                    modelPost.IdPost = post.Id;
                    modelPost.Precio = post.Precio;
                    modelPost.Moneda = post.Moneda;
                    modelPost.Descripcion = post.Descripcion;
                    modelPost.Imagen = null;
                    modelPost.Titulo = post.Titulo;
                    modelPost.PermiteCanjes = post.PermiteCanjes;
                    modelPost.PermiteContraOfertas = post.PermiteContraofertas;
                    modelPost.PermiteCuotas = post.PermiteCuotas;
                    modelPost.Fecha = post.FechaPublicacion;
                    modelPost.Prioridad = post.Prioridad;
                    modelPost.Localidad = post.Localidad;
                    listaPosts.Add(modelPost);
                }
            }
            FiltrosViewModels filtros = new FiltrosViewModels();
            filtros.MontoDesde = null;
            filtros.MontoHasta = null;
            filtros.PalabrasClave = "";
            model.Filtros = filtros;
            model.ActionName = "Index";
            model.ListaPosts = listaPosts;
            model.ListaPosts = model.ListaPosts.OrderByDescending(x => x.Prioridad).ThenByDescending(x => x.Fecha).ToList();
            return View(model);
        }

        public ActionResult FiltrarRegistros(int idCat, int idSubCat, int? montoDesde, int? montoHasta, string palabrasClave, bool contraofertas, bool canjes, bool cuotas, int pagina = 1)
        {
            CategoryViewModels model = new CategoryViewModels();
            model.ActionName = "FiltrarRegistros";
            string categoria = db.Categorias.Where(x => x.Id == idCat && x.IdEstado == 1).FirstOrDefault().Descripcion;
            string subcategoria = null;
            List<Posts> ListaPosts = new List<Posts>();
            if (idSubCat > -1)
            {
                subcategoria = db.SubCategorias.Where(x => x.Id == idSubCat && x.IdEstado == 1).FirstOrDefault().Descripcion;
                ListaPosts = db.Posts.Where(x => x.IdCategoria == idCat && x.IdSubCategoria == idSubCat).OrderBy(x => x.Id).Skip((pagina - 1) * Convert.ToInt32(ConfigurationManager.AppSettings["PostsPorPagina"])).Take(Convert.ToInt32(ConfigurationManager.AppSettings["PostsPorPagina"])).ToList();
            }
            else //Eligio "Todas" en subcategorias
            {
                ListaPosts = db.Posts.Where(x => x.IdCategoria == idCat).OrderBy(x => x.Id).Skip((pagina - 1) * Convert.ToInt32(ConfigurationManager.AppSettings["PostsPorPagina"])).Take(Convert.ToInt32(ConfigurationManager.AppSettings["PostsPorPagina"])).ToList();
            }

            ////FILTROS////
           
            //Si filtra por monto
            if (montoDesde != null && montoHasta != null)
            {
                ListaPosts = ListaPosts.Where(x => x.Precio >= montoDesde && x.Precio <= montoHasta).ToList();
            }
            //Si filtra por palabras clave
            if (!string.IsNullOrEmpty(palabrasClave))
            {
                List<string> ListaPalabrasClave = new List<string>(); //Particionamos las palabras clave en una lista para buscarlas por separado
                ListaPalabrasClave = palabrasClave.Split(' ').ToList();
                List<Posts> ListaPostsAux = new List<Posts>();
                foreach (var word in ListaPalabrasClave)
                {
                    List<Posts> ListaPostsAuxWord = new List<Posts>();
                    ListaPostsAuxWord = ListaPosts.Where(x => x.Titulo.ToLower().Contains(word.ToLower()) || x.Descripcion.ToLower().Contains(word.ToLower())).ToList();
                    foreach (var item in ListaPostsAuxWord)
                    {
                        if (item != null)
                        {
                            bool existe_post = ListaPostsAux.Any(x => x.Id == item.Id);
                            if (!existe_post)
                            {
                                ListaPostsAux.Add(item);
                            }
                        }
                    }
                }
                ListaPosts = ListaPostsAux;
            }
            //Si filtra por contraoferta
            if (contraofertas)
            {
                ListaPosts = ListaPosts.Where(x => x.PermiteContraofertas).ToList();
            }
            //Si filtra por canje
            if (canjes)
            {
                ListaPosts = ListaPosts.Where(x => x.PermiteCanjes).ToList();
            }
            //Si filtra por cuotas
            if (cuotas)
            {
                ListaPosts = ListaPosts.Where(x => x.PermiteCuotas).ToList();
            }
            model.TotalDeRegistros = db.Posts.Count();
            model.PaginaActual = pagina;
            model.RegistrosPorPagina = Convert.ToInt32(ConfigurationManager.AppSettings["PostsPorPagina"]);
            model.Categoria = categoria;
            model.SubCategoria = subcategoria;
            model.IdCategoria = idCat;
            model.IdSubCategoria = idSubCat;

            List<PostExtendedViewModels> listaPosts = new List<PostExtendedViewModels>();
            foreach (var post in ListaPosts)
            {
                Imagenes imgPost = db.Imagenes.Where(x => x.IdPost == post.Id).FirstOrDefault();
                if (imgPost != null)
                {
                    PostExtendedViewModels modelPost = new PostExtendedViewModels();
                    modelPost.IdPost = post.Id;
                    modelPost.Precio = post.Precio;
                    modelPost.Moneda = post.Moneda;
                    modelPost.Descripcion = post.Descripcion;
                    modelPost.Imagen = imgPost.imagen;
                    modelPost.Titulo = post.Titulo;
                    modelPost.PermiteCanjes = post.PermiteCanjes;
                    modelPost.PermiteContraOfertas = post.PermiteContraofertas;
                    modelPost.PermiteCuotas = post.PermiteCuotas;
                    modelPost.Prioridad = post.Prioridad;
                    modelPost.Localidad = post.Localidad;
                    modelPost.Fecha = post.FechaPublicacion;
                    listaPosts.Add(modelPost);
                }
                else
                {
                    PostExtendedViewModels modelPost = new PostExtendedViewModels();
                    modelPost.IdPost = post.Id;
                    modelPost.Precio = post.Precio;
                    modelPost.Moneda = post.Moneda;
                    modelPost.Descripcion = post.Descripcion;
                    modelPost.Imagen = null;
                    modelPost.Titulo = post.Titulo;
                    modelPost.PermiteCanjes = post.PermiteCanjes;
                    modelPost.PermiteContraOfertas = post.PermiteContraofertas;
                    modelPost.PermiteCuotas = post.PermiteCuotas;
                    modelPost.Prioridad = post.Prioridad;
                    modelPost.Localidad = post.Localidad;
                    modelPost.Fecha = post.FechaPublicacion;
                    listaPosts.Add(modelPost);
                }
            }
            FiltrosViewModels filtros = new FiltrosViewModels();
            filtros.MontoDesde = montoDesde;
            filtros.MontoHasta = montoHasta;
            filtros.PalabrasClave = palabrasClave;
            filtros.Contraofertas = contraofertas;
            filtros.Canjes = canjes;
            filtros.Cuotas = cuotas;
            model.Filtros = filtros;
            model.ListaPosts = listaPosts;
            model.ListaPosts = model.ListaPosts.OrderByDescending(x => x.Prioridad).ThenByDescending(x => x.Fecha).ToList();
            return View("Index", model);
        }

        public ActionResult _Sidebar()
        {
            List<Categorias> categorias = new List<Categorias>();
            List<SubCategorias> subcategorias = new List<SubCategorias>();
            categorias = db.Categorias.Where(x => x.IdEstado == 1).ToList();
            subcategorias = db.SubCategorias.Where(x => x.IdEstado == 1).ToList();

            SidebarViewModels CatViewModel = new SidebarViewModels();
            CatViewModel.Categorias = categorias;
            CatViewModel.Subcategorias = subcategorias;
            return PartialView("_Sidebar", CatViewModel);
        }
    }
}