﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class ComentarioViewModel //Se utiliza para guardar un comentario recien creado
    {
        public int IdPost { get; set; }
        public string UserName { get; set; } //No pongo el id porque Identity solo tiene el UserName
        public string Contenido { get; set; }
        public int IdEstado { get; set; }
    }
}