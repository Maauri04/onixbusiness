﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class ActualizarCanjeViewModels
    {
        public int idCanje { get; set; }
        public string userNameVendedor { get; set; }
        public bool aceptaCanje { get; set; }
    }
}