﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class UserViewModels
    {
        public string IdUsuario { get; set; }
        public string Nombre { get; set; }
        public byte[] Imagen { get; set; }
        public List<Notificaciones> Notificaciones = new List<Notificaciones>();
    }
}