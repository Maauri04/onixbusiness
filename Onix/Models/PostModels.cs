﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class PostModels
    {
        //Datos de referencia (NO se guardan en la db, se buscan con los Id de abajo)
        public string Categoria { get; set; }
        public string SubCategoria { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public string Celular { get; set; }
        public string Telefono { get; set; }
        public string Domicilio { get; set; }
        public string LocalidadContacto { get; set; }

        public string Titulo { get; set; }
        public int Precio { get; set; }
        public string Moneda { get; set; }
        public string Localidad { get; set; }
        public string Descripcion { get; set; }
        public bool PermiteContraofertas { get; set; }
        public bool PermiteCuotas { get; set; }
        public bool PermiteCanjes { get; set; }
        public bool PermitePreguntas { get; set; }
        public int Visitas { get; set; }
        public DateTime FechaPublicacion { get; set; }
        public string IdUsuario { get; set; }
        public int IdEstado { get; set; }
        public int IdCategoria { get; set; }
        public bool ApellidoVisible { get; set; }
        public bool EmailVisible { get; set; }
        public bool CelularVisible { get; set; }
        public bool TelefonoFijoVisible { get; set; }
        public bool DomicilioVisible { get; set; }
        public bool LocalidadContactoVisible { get; set; } //localidad del contacto visible
        public string[][] Caracteristicas { get; set; }
        public string Portada { get; set; }
    }
}