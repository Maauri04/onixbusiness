﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class ContraofertaViewModel
    {
        public int IdPost { get; set; }
        public string IdUsuario { get; set; }
        public int PrecioOfertado { get; set; }
        public int PrecioPost { get; set; }
        public string EstadoContraOferta { get; set; }
    }
}