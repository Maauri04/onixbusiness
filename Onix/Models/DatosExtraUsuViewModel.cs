﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class DatosExtraUsuViewModel //Se utilizan para completar los datos faltantes luego del registro (cuadro modal)
    {
        public string IdUsuario { get; set; }
        public string Direccion { get; set; }
        public string Celular { get; set; }
        public string Localidad { get; set; }
    }
}