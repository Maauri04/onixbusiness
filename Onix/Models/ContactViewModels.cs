﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Datos;

namespace Onix.Models
{
    public class ContactViewModels
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Email { get; set; }
        public string Celular { get; set; }
        public string Telefono { get; set; }
        public string Domicilio { get; set; }
        public string Localidad { get; set; }
        public PautasPublicitarias PautaPublicitaria { get; set; }
        public bool PuedePublicar { get; set; }
    }
}