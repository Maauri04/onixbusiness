﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class ComentarioViewModels //Se utiliza para mostrar comentarios de los posts
    {
        public byte[] Imagen { get; set; }
        public string Nombre { get; set; }
        public DateTime Fecha { get; set; }
        public string Contenido { get; set; }
        public bool EsVendedor { get; set; }
    }
}