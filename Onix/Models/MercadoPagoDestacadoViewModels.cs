﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class MercadoPagoDestacadoViewModels
    {
        public string collection_id { get; set; }
        public string collection_status { get; set; }
        public string payment_type { get; set; }
        public string merchant_order_id { get; set; }
        public string preference_id { get; set; }
        public string site_id { get; set; }
        public string processing_mode { get; set; }
        public string merchant_account_id { get; set; }
        public byte[] imagen_destacado { get; set; }
        public string tipo_destacado { get; set; }
        public string titulo { get; set; }
    }
}