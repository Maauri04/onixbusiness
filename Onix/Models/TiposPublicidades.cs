﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public static class TiposPublicidades
    {
        public const string MediaVariable = "Media Variable";
        public const string MediaFija = "Media Fija";
        public const string LargaVariable = "Larga Variable";
        public const string LargaFija = "Larga Fija";
    }
}