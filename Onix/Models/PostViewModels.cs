﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class PostViewModels
    {
        public int IdPost { get; set; }
        public string IdUsuario { get; set; }
        public string Titulo { get; set; }
        public int Precio { get; set; }
        public string Moneda { get; set; }
        public DateTime Fecha { get; set; }
        public int Visitas { get; set; }
        public List<byte[]> ListaImagenes = new List<byte[]>();
        public string Localidad { get; set; }
        public string Descripcion { get; set; }
        public bool PermiteContraofertas { get; set; }
        public bool PermiteCuotas { get; set; }
        public bool PermiteCanjes { get; set; }
        public bool PermitePreguntas { get; set; }
        public string NombreUsuario { get; set; }
        public string ApellidoUsuario { get; set; }
        public string Email { get; set; }
        public string Celular { get; set; }
        public string TelefonoFijo { get; set; }
        public string Direccion { get; set; }
        public string LocalidadContacto { get; set; }
        public string Categoria { get; set; }
        public string SubCategoria { get; set; }
        public CaracteristicasAgro Agro { get; set; }
        public CaracteristicasAnimales Animales { get; set; }
        public CaracteristicasAutomotores Automotores { get; set; }
        public CaracteristicasBebesNinos BebesNinos { get; set; }
        public CaracteristicasElectronica Electronica { get; set; }
        public CaracteristicasEmpleos Empleos { get; set; }
        public CaracteristicasHogarMuebles HogarMuebles { get; set; }
        public CaracteristicasInmuebles Inmuebles { get; set; }
        public CaracteristicasVarios Varios { get; set; }
        public bool ApellidoVisible { get; set; }
        public bool EmailVisible { get; set; }
        public bool CelularVisible { get; set; }
        public bool TelefonoVisible { get; set; }
        public bool DomicilioVisible { get; set; }
        public bool LocalidadVisible { get; set; } //localidad del contacto visible
        public byte[] ImagenUsuario { get; set; }
        public List<ComentarioViewModels> ListaComentarios = new List<ComentarioViewModels>();
        public List<ContraOfertasViewModels> ListaContraOfertas = new List<ContraOfertasViewModels>();
        public List<CanjesViewModels> ListaCanjes = new List<CanjesViewModels>();
        public int ValoracionesPositivas { get; set; }
        public int CantidadPublicaciones { get; set; }
        public int? Prioridad { get; set; }
    }
}