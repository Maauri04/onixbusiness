﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Datos;

namespace Onix.Models
{
    public class SidebarViewModels
    {
        public IEnumerable<Categorias> Categorias { get; set; }
        public IEnumerable<SubCategorias> Subcategorias { get; set; }
        public string Cat_seleccionada { get; set; }
    }
}