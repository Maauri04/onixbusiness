﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class CategoryViewModels : PaginationViewModels
    {
        public int IdCategoria { get; set; }
        public int IdSubCategoria { get; set; }
        public string Categoria { get; set; }
        public string SubCategoria { get; set; }
        public List<PostExtendedViewModels> ListaPosts = new List<PostExtendedViewModels>();
        public FiltrosViewModels Filtros { get; set; }
        public string ActionName { get; set; }
    }
}