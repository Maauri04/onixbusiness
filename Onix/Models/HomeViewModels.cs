﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class HomeViewModels : PaginationViewModels
    {
        public List<PostHomeViewModels> ListaPosts = new List<PostHomeViewModels>();
        public int? idEstado { get; set; }
    }
}