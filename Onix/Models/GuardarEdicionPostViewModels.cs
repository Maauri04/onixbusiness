﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class GuardarEdicionPostViewModels
    {
        public int IdPost { get; set; }
        public string Categoria { get; set; }
        public string Titulo { get; set; }
        public int Precio { get; set; }
        public string Moneda { get; set; }
        public string Localidad { get; set; }
        public string Descripcion { get; set; }
        public string NombreUsuario { get; set; }
        public string ApellidoUsuario { get; set; }
        public string Email { get; set; }
        public string Celular { get; set; }
        public string TelefonoFijo { get; set; }
        public string Direccion { get; set; }
        public string LocalidadContacto { get; set; }
        public bool PermiteCanjes { get; set; }
        public bool PermiteCuotas { get; set; }
        public bool PermiteContraofertas { get; set; }
        public bool PermitePreguntas { get; set; }
        public bool ApellidoVisible { get; set; }
        public bool EmailVisible { get; set; }
        public bool CelularVisible { get; set; }
        public bool TelefonoVisible { get; set; }
        public bool DomicilioVisible { get; set; }
        public bool LocalidadVisible { get; set; }
        public string[][] CaracteristicasPost { get; set; }
    }
}