﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class DestacarAvisoViewModels
    {
        public int IdPost { get; set; }
        public string IdUsuario { get; set; }
        public byte[] Imagen { get; set; }
        public string Titulo { get; set; }
        public bool estaDestacado { get; set; }
    }
}