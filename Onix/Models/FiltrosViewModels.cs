﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class FiltrosViewModels
    {
        public int? MontoDesde { get; set; }
        public int? MontoHasta { get; set; }
        public string PalabrasClave { get; set; }
        public bool Contraofertas { get; set; }
        public bool Canjes { get; set; }
        public bool Cuotas { get; set; }
    }
}