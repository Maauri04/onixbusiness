﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class ActualizarContraOfertaViewModels
    {
        public int idContraoferta { get; set; }
        public string userNameVendedor { get; set; }
        public bool aceptaContraoferta { get; set; }
    }
}