﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class PerfilUsuarioViewModels //Se utiliza para ver el perfil publico de un usuario
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Localidad { get; set; }
        public byte[] Imagen { get; set; }
        public int CantidadPublicaciones { get; set; }
        public bool? BrindaBuenaAtencion { get; set; }
        public bool? ValidadoConFacebook { get; set; }
        public int ValoracionesPositivas { get; set; }
        public List<PostExtendedViewModels> ListaPosts = new List<PostExtendedViewModels>();
        public string Email { get; set; } //Se utiliza para comprobar con el Identity.UserName en la vista, para mostrar el Editar Perfil
    }
}