﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class ContracanjeViewModel
    {
        public int IdPost { get; set; }
        public string IdUsuario { get; set; }
        public int Precio { get; set; }
        public bool CanjeParcial { get; set; }
        public string Descripcion { get; set; }
        public string EstadoCanje { get; set; }
    }
}