﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class GuardarDestacadoViewModels
    {
        public int IdPost { get; set; }
        public int Prioridad { get; set; }
        public DateTime? FechaDestacadoDesde { get; set; }
        public DateTime? FechaDestacadoHasta { get; set; }
        public int Dias { get; set; }
        public string NroOperacion { get; set; }
    }
}