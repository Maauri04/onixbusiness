﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class BuscarViewModel : PaginationViewModels
    {
        public string palabrasClave { get; set; }
        public List<PostExtendedViewModels> ListaPosts = new List<PostExtendedViewModels>();
    }
}