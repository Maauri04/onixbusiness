﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public static class StaticCategorias
    {
        public const string Agro = "AGRO";
        public const string Automotores = "AUTOMOTORES";
        public const string Animales = "ANIMALES";
        public const string BebesNinos = "BEBES Y NIÑOS";
        public const string Electronica = "ELECTRÓNICA";
        public const string HogarMuebles = "HOGAR Y MUEBLES";
        public const string Inmuebles = "INMUEBLES";
        public const string Empleos = "EMPLEOS";
        public const string Varios = "VARIOS";
        public const string Servicios = "SERVICIOS";
    }
}