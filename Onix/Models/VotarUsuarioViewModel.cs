﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class VotarUsuarioViewModel
    {
        public string IdUsuarioOrigen { get; set; }
        public string IdUsuarioDestino { get; set; }
        public DateTime? Fecha { get; set; }
        public bool VotoPositivo { get; set; }
    }
}