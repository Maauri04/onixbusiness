﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class ImagenViewModels
    {
        public byte[] Imagen { get; set; }
        public string Nombre { get; set; }
    }
}