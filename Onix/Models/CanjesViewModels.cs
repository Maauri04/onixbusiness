﻿using Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class CanjesViewModels //se usa para mostrar los COLLAPSE del post
    {
        public int Id { get; set; }
        public int IdPost { get; set; }
        public int Precio { get; set; }
        public string Moneda { get; set; }
        public string IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string ApellidoUsuario { get; set; }
        public string Descripcion { get; set; }
        public string EstadoCanje { get; set; }
        public List<ImagenesCanjes> ListaImagenesCanje = new List<ImagenesCanjes>();
    }
}