﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class PostExtendedViewModels //Se utiliza para mostrar los post en formato como los de Categoria, o los que se ven en Perfil de usuarios
    {
        public int IdPost { get; set; }
        public int IdCategoria { get; set; }
        public string Titulo { get; set; }
        public byte[] Imagen { get; set; }
        public string Moneda { get; set; }
        public int Precio { get; set; }
        public string Descripcion { get; set; }
        public bool PermiteCuotas { get; set; }
        public bool PermiteContraOfertas { get; set; }
        public bool PermiteCanjes { get; set; }
        public DateTime Fecha { get; set; }
        public int? Prioridad { get; set; }
        public string Localidad { get; set; }
    }
}