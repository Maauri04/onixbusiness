﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class ContraOfertasViewModels //Se usa para mostrar los COLLAPSE del post
    {
        public int Id { get; set; }
        public int IdPost { get; set; }
        public int Precio { get; set; }
        public string IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string ApellidoUsuario { get; set; }
    }
}