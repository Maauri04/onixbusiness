﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class FacebookViewModels
    {
        public string ImagenUsuario { get; set; }
        public string EmailUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string IdUsuarioFacebook { get; set; }
    }
}