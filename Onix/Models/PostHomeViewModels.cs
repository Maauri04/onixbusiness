﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Onix.Models
{
    public class PostHomeViewModels
    {
        public int idPost { get; set; }
        public byte[] Imagen { get; set; }
        public string Titulo { get; set; }
        public int? Prioridad { get; set; }
        public DateTime FechaPublicacion { get; set; }
    }
}