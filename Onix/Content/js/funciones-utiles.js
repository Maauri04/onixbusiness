﻿function soloNumeros(evt) {
    if (window.event) { // IE
        keyNum = evt.keyCode;
    } else {
        keyNum = evt.which;
    }

    if (keyNum >= 48 && keyNum <= 57) {
        return true;
    } else {
        return false;
    }
}


//FACEBOOK LOGIN
var app_id = '587764898829680';
var scopes = 'public_profile, email';

var btn_login = '<a href="#" id="login" class="btn btn-facebook btn-user btn-block" style="color:white; background-color:#385898"><i class="fab fa-facebook-f fa-fw"></i>Ingresar con Facebook</a>';

var div_session = "<div id='facebook-session' style='display:none'>" +
    "<strong></strong>" +
    "<img id='imgFace'>" +
    "<a href='#' id='logout' class='btn btn-danger'>Cerrar sesión</a>" +
    "</div>";

window.fbAsyncInit = function () {

    FB.init({
        appId: app_id,
        status: true,
        cookie: true,
        xfbml: true,
        version: 'v2.8'
    });


    //FB.getLoginStatus(function (response) {
    //    statusChangeCallback(response, function () { });
    //});
};

var statusChangeCallback = function (response, callback) {
    console.log(response);

    if (response.status === 'connected') {
        getFacebookData();
    } else {
        callback(false);
    }
}

var checkLoginState = function (callback) {
    FB.getLoginStatus(function (response) {
        callback(response);
    });
}

function base64ToArrayBuffer(base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
}

var getFacebookData = function () {
    Swal.fire({
        title: 'Validando con Facebook..',
        showCancelButton: false,
        allowOutsideClick: false,
        onBeforeOpen: () => {
            Swal.showLoading()
        }
    });
    FB.api('/me', { "fields": "name, email" }, function (response) {
        $('#login').after(div_session);
        //$('#login').remove();
        $('#facebook-session img').attr('src', 'https://graph.facebook.com/' + response.id + '/picture?type=large');
        let ImagenUsuario = "";
        const toDataURL = url => fetch(url)
            .then(response => response.blob())
            .then(blob => new Promise((resolve, reject) => {
                const reader = new FileReader()
                reader.onloadend = () => resolve(reader.result)
                reader.onerror = reject
                reader.readAsDataURL(blob)
            }))


        toDataURL($('#facebook-session img').attr('src'))
            .then(dataUrl => { //dataUrl es la imagen decodificada
                let ImagenUsuario = dataUrl.replace(/^data:image\/(png|jpeg);base64,/, "");
                let EmailUsuario = response.email;
                let NombreUsuario = response.name;
                let IdUsuarioFacebook = response.id;
                //Hacer el login con bd
                let data = {};
                let options = {};
                data.ImagenUsuario = ImagenUsuario;
                data.EmailUsuario = EmailUsuario;
                data.NombreUsuario = NombreUsuario;
                data.IdUsuarioFacebook = IdUsuarioFacebook;
                options.url = "/Account/ExternalLoginCallback";
                options.type = "POST";
                options.data = JSON.stringify({ "data": data });
                options.dataType = "json";
                options.contentType = "application/json";
                options.success = function () {
                    swal.close();
                    window.location.href = "/Home/Index";
                };
                options.error = function (err) {
                    swal.close();
                    Swal.fire({
                        title: 'Se ha producido un error',
                        icon: 'error',
                        confirmButtonColor: '#59287a',
                        text: 'Por favor, intentá ingresar con tu cuenta de ONIX',
                        backdrop: `
                                rgba(206, 9, 9, 0.6)
                                `
                    })
                };
                $.ajax(options);
            })       
    });
}

var validarFacebookData = function () {
    Swal.fire({
        title: 'Validando con Facebook..',
        showCancelButton: false,
        onBeforeOpen: () => {
            Swal.showLoading()
        }
    });
    FB.api('/me', { "fields": "name,email" }, function (response) {
        let EmailUsuario = response.email;
        let NombreUsuario = response.name;
        let IdUsuarioFacebook = response.id;
        let data = {};
        let options = {};
        data.ImagenUsuario = null;
        data.EmailUsuario = EmailUsuario;
        data.NombreUsuario = NombreUsuario;
        data.IdUsuarioFacebook = IdUsuarioFacebook;
        options.url = "/Account/ExternalLoginCallback";
        options.type = "POST";
        options.data = JSON.stringify({ "data": data, "EsValidarUsuario": true });
        options.dataType = "json";
        options.contentType = "application/json";
        options.success = function (response) {
            swal.close();
            confetti.start(2200, 100, 150);
            Swal.fire({
                title: '¡Te validaste con Facebook!',
                icon: 'success',
                confirmButtonColor: '#59287a',
                text: "Los demás usuarios tendrán más seguridad y confianza al ver tu perfil.",
                backdrop: `
                rgba(9, 206, 45, 0.6)
                `
            }).then((avanzar) => {
                if (avanzar.value) {
                    window.location.reload();
                }
            });
        };
        options.error = function (err) {
            swal.close();
            Swal.fire({
                title: 'Se ha producido un error',
                icon: 'error',
                confirmButtonColor: '#59287a',
                text: 'No hemos podido validar tu identidad con Facebook. Intentá nuevamente más tarde o contactate con ONIX si el problema persiste.',
                backdrop: `
                            rgba(206, 9, 9, 0.6)
                            `
            });
        };
        $.ajax(options);
    });
}

var facebookLogin = function () {
    checkLoginState(function (data) {
        FB.login(function (response) {
            if (response.status === 'connected') {
                getFacebookData();
            }
        }, { scope: scopes });
    })
}

var facebookLoginValidacion = function () {
    checkLoginState(function (data) {
        FB.login(function (response) {
            if (response.status === 'connected') {
                validarFacebookData();
            }
        }, { scope: scopes });
    })
}

var facebookLogout = function () {
    checkLoginState(function (data) {
        if (data.status === 'connected') {
            FB.logout(function (response) {
                $('#facebook-session').before(btn_login);
                $('#facebook-session').remove();
            })
        }
    })

}



$(document).on('click', '#login', function (e) {
    e.preventDefault();
    facebookLogin();
});

$(document).on('click', '#validar', function (e) {
    e.preventDefault();
    facebookLoginValidacion();
});

$(document).on('click', '#logout', function (e) {
    e.preventDefault();

    if (confirm("¿Está seguro?"))
        facebookLogout();
    else
        return false;
})


