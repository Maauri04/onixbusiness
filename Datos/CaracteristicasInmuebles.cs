//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class CaracteristicasInmuebles
    {
        public int Id { get; set; }
        public int IdPost { get; set; }
        public string Tipo { get; set; }
        public string Operacion { get; set; }
        public string Domicilio { get; set; }
        public Nullable<bool> Patio { get; set; }
        public Nullable<bool> Cochera { get; set; }
        public Nullable<int> Superficie { get; set; }
        public Nullable<int> Banos { get; set; }
        public Nullable<int> Habitaciones { get; set; }
        public Nullable<bool> Cloacas { get; set; }
        public Nullable<bool> GasNatural { get; set; }
        public Nullable<bool> AguaCaliente { get; set; }
        public string TipoVendedor { get; set; }
        public string Condicion { get; set; }
    
        public virtual Posts Posts { get; set; }
    }
}
